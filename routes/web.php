<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/', function () {
//    return DB::table('core_users')->get();
// });
Route::get('/login-report',[App\Http\Controllers\ReportHomeController::class, 'login']);
Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', [App\Http\Controllers\ReportHomeController::class, 'index'])->name('home');
    Route::get('/error', function () {
        return view('pages.errors.error');
    })->name('error');
    Route::resource('departments', App\Http\Controllers\DepartmentController::class);
    Route::resource('parties', App\Http\Controllers\PartyController::class);
    Route::get('/reports/loan-request', [App\Http\Controllers\LoanRequestController::class, 'index']);
    Route::get('/reports/loan-created', [App\Http\Controllers\LoanCreatedController::class, 'index']);
    Route::get('/reports/loan-released', [App\Http\Controllers\LoanReleasedController::class, 'index']);
    Route::get('/reports/repaid-amounts', [App\Http\Controllers\RepaidAmountController::class, 'index']);
    Route::get('/reports/interest-accumulated', [App\Http\Controllers\InterestAccumulatedController::class, 'index']);
    Route::get('/reports/saving-account-opened', [App\Http\Controllers\SavingAccountOpenedController::class, 'index']);
    Route::get('/reports/withdrawals', [App\Http\Controllers\WithdrawalController::class, 'index']);
    Route::get('/reports/deposits', [App\Http\Controllers\DepositController::class, 'index']);
    Route::get('/reports/saving-account-closed', [App\Http\Controllers\SavingAccountClosedController::class, 'index']);
    Route::get('/reports/investment-created', [App\Http\Controllers\InvestmentCreatedController::class, 'index']);
    Route::get('/reports/interest-given', [App\Http\Controllers\InterestGivenController::class, 'index']);
    //Route::get('/reports/investment-account-opened', [App\Http\Controllers\InvestmentAccountOpenedController::class, 'index']);
    Route::get('/reports/investment-account-closed', [App\Http\Controllers\InvestmentAccountClosedController::class, 'index']);
    //Route::get('/reports/collection-report', [App\Http\Controllers\CollectionReportController::class, 'collectionReport']);
    Route::get('/reports/collection/officer-wise', [App\Http\Controllers\CollectionReportController::class, 'officerWise']);
    Route::get('/reports/collection/center-wise', [App\Http\Controllers\CollectionReportController::class, 'centerWise']);
    Route::get('/reports/collection/department-wise', [App\Http\Controllers\CollectionReportController::class, 'departmentWise']);

    Route::get('/reports/loans-report', [App\Http\Controllers\LoanReportController::class, 'detailed']);
    Route::get('/reports/loans-combined-summary-report', [App\Http\Controllers\LoanReportController::class, 'combinedSummary']);
    Route::get('/reports/loans-summary-report', [App\Http\Controllers\LoanReportController::class, 'summary']);
    Route::get('/reports/release-report', [App\Http\Controllers\LoanReportController::class, 'release']);
    Route::get('/reports/release-summary', [App\Http\Controllers\LoanReportController::class, 'releaseSummary']);
    Route::get('/reports/collection-report', [App\Http\Controllers\LoanReportController::class, 'collectionDetailed']);
    Route::get('/reports/collection-summary-report', [App\Http\Controllers\LoanReportController::class, 'collectionSummary']);
    Route::get('/reports/debtor-balance-report', [App\Http\Controllers\LoanReportController::class, 'debtorBalance']);
    Route::get('/reports/npl-report', [App\Http\Controllers\LoanReportController::class, 'npl']);
    Route::get('/reports/fees-report', [App\Http\Controllers\LoanReportController::class, 'fees']);
    Route::get('/reports/product-analysis-report', [App\Http\Controllers\LoanReportController::class, 'productAnalysis']);
    Route::get('/reports/csu-analysis-report', [App\Http\Controllers\LoanReportController::class, 'csuAnalysis']);
    Route::get('/reports/branch-analysis-report', [App\Http\Controllers\LoanReportController::class, 'branchAnalysis']);
    

});



