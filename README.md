<p align="center">Apptimo Core</p>


## System Setup - Laravel
### Installation

- clone https://gitlab.com/internal-apptimus-projects/apptimo-components/apptimo-core.git.
- cd /system setup.
- `composer install`
- cp .env.example .env
- configure the database connection in the .env file
- `php artisan key generate`
- `php artisan db:seed`
- `php artisan serve`

#### Auth Config
- `php artisan passport:client --personal`
- configure the client id , secret connection in the .env file
```
PASSPORT_PERSONAL_ACCESS_CLIENT_ID= [client id]
PASSPORT_PERSONAL_ACCESS_CLIENT_SECRET= [client id]
CLIENT_NAME="CLIENT NAME"
```