<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission1 = DB::table('core_permissions')->where('name','add_users')->first();
        $permission2 = DB::table('core_permissions')->where('name','view_users')->first();
        $role = DB::table('core_roles')->where('name','admin')->first();

        // $this->command->info($role);

        DB::table('core_roles_permissions')->insert([
            'permissions_id' => $permission1->id,
            'roles_id' => $role->id,
        ]);

        DB::table('core_roles_permissions')->insert([
            'permissions_id' => $permission2->id,
            'roles_id' => $role->id,
        ]);
    }
}
