<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $module = DB::table('core_modules')->where('name','HR')->first();

        DB::table('core_permissions')->insert([
            'id' => uuid4(),
            'display_name' => 'View Users',
            'name' => 'view_users',
            'group_by' => 'users',
            'modules_id' => $module->id,
        ]);

        DB::table('core_permissions')->insert([
            'id' => uuid4(),
            'display_name' => 'Add Users',
            'name' => 'add_users',
            'group_by' => 'users',
            'modules_id' => $module->id,
        ]);
    }
}
