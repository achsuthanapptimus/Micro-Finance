<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('core_roles')->insert([
            'id' => uuid4(),
            'name' => 'admin',
            'type' => 'Admin',
        ]);

        
    }
}
