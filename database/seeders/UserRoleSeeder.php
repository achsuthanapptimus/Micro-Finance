<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = DB::table('core_roles')->where('name','admin')->first();
        $department = DB::table('core_departments')->where('code_name','SE-100000')->first();
        $user = DB::table('core_users')->where('username','admin')->first();
        $this->command->info($user->id);

        DB::table('core_users_roles')->insert([
            'roles_id' => $role->id,
            'users_id' => $user->id,
            'departments_id' => $department->id,
            'is_main'=>1,
        ]);
    }
}
