<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('core_departments')->insert([
            'id' => uuid4(),
            'name' => 'Jaffna',
            'code_name' => 'SE-100000',
            'type' => 'department',
        ]);
    }
}
