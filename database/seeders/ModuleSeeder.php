<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('core_modules')->insert([
            'id' => uuid4(),
            'name' => 'HR',
            'login_url' => '',
        ]);

        DB::table('core_modules')->insert([
            'id' => uuid4(),
            'name' => 'Account',
            'login_url' => '',
        ]);
    }
}
