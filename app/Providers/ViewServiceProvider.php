<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Auth,Session,View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('include.header', function ($view) {
            $users = Auth::user()->userRole;
            $departments = [];
            foreach ($users as $key => $value) {
                $departments[] = [
                    "roles_id" => $value->roles_id,
                    "role" => $value->role->name,
                    "departments_id" => $value->departments_id,
                    "department" => $value->department->name,
                    "is_main" => $value->is_main,
                ];
            }
            if(Session::has('department')) {
                $current = Session::get('department');
                $current_department = collect($departments)->where('departments_id', $current)->first();
            }else{
                $current =  Auth::user()->mainRole->first();
                $current_department = collect($departments)->where('departments_id', $current->pivot->departments_id)->first();
            }
// dd($current_department);
            $view->with('departments',$departments)->with('current_department',$current_department);
        });

    }
}
