<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Passport::$tokenModel = '\App\Models\AccessToken';
        Passport::$authCodeModel = '\App\Models\AuthAuthCode';
        Passport::$clientModel = '\App\Models\AuthClient';
        Passport::$personalAccessClientModel = '\App\Models\AuthPersonalAccessClient';
        Passport::$refreshTokenModel = '\App\Models\AuthRefreshToken';
        Schema::defaultStringLength(191);
    }
}
