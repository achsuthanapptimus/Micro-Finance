<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Http\Requests\FileUploadRequest;
use Illuminate\Support\Str;
use Session;
use Redirect;
use Validator;
use File;



class FIleUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FileUploadRequest $request)
    {
        try{

            $directory = storage_path().$request->type;

            if(!File::isDirectory($directory)){
                File::makeDirectory($directory);

            }

            if ($request->hasFile('file')) {
                $request->file;
                $file = $request->file('file');
                $fileName= time().Str::random(25).".".$file->getClientOriginalExtension();
                $file->move($directory, $fileName);
                
                 return response()->json([
                    "success" => true,
                    "message" => "Operation successful.",
                    "data" => $request->type .'\\'. $fileName,
                ],200);
                
            }
           
        }catch (\Exception $th) {
            dd($th->getMessage());
            return response()->json([
                "success" => false,
                "message" => "Operation failed.",
                "data" => null,
            ],200);
        }
    }
}
