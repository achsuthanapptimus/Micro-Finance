<?php

namespace App\Http\Controllers;
use App\Models\LoanCreated;

use Illuminate\Http\Request;
use DB;
use DateTime;

class LoanReleasedController extends Controller
{
    //
    public function index(Request $request)
    {

        $dt = new DateTime();
        $search="";
        $xaxisVals = array();
        $yaxisVals = array();


        if($request->get('fdate')){
            $fdate = $request->get('fdate');
        }else{
            $fdate = $dt->format('Y-m-d');
        }

        if($request->get('tdate')){
            $tdate = $request->get('tdate');
        }else{
            $tdate = $dt->format('Y-m-d');
        }

        $grouping=$request->grouping;
        
        if($request->grouping == "day-wise"){
             $loanReleased = DB::table('bank_loan_accounts')
            ->select(DB::raw('DATE_FORMAT(initiated_date, "%Y-%m-%d - %W") AS grp,SUM(amount) as amount,COUNT(id) as val'))
            ->groupBy(DB::raw('DATE_FORMAT(initiated_date, "%Y-%m-%d - %W")'))
            ->where('initiated_date', '>=', $fdate)
            ->where('initiated_date', '<=', $tdate)
            ->where('status', '=', 'released')
            ->get();

            foreach($loanReleased as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.loan-released',compact('loanReleased','grouping','fdate','tdate','xaxisVals','yaxisVals'));

        }
        elseif($request->grouping == "month-wise"){
            $loanReleased = DB::table('bank_loan_accounts')
            ->select(DB::raw('DATE_FORMAT(initiated_date, "%Y-%m - %M") AS grp,SUM(amount) as amount,COUNT(id) as val'))
            ->groupBy(DB::raw('DATE_FORMAT(initiated_date, "%Y-%m - %M")'))
            ->where('initiated_date', '>=', $fdate)
            ->where('initiated_date', '<=', $tdate)
            ->where('status', '=', 'released')
            ->get();

            foreach($loanReleased as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.loan-released',compact('loanReleased','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "year-wise"){
            $loanReleased = DB::table('bank_loan_accounts')
            ->select(DB::raw('DATE_FORMAT(initiated_date, "%Y") AS grp,SUM(amount) as amount,COUNT(id) as val'))
            ->groupBy(DB::raw('DATE_FORMAT(initiated_date, "%Y")'))
            ->where('initiated_date', '>=', $fdate)
            ->where('initiated_date', '<=', $tdate)
            ->where('status', '=', 'released')
            ->get();

            foreach($loanReleased as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.loan-released',compact('loanReleased','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "department-wise"){
            $loanReleased = DB::table('bank_loan_accounts')
            ->leftjoin('core_departments','core_departments.id' ,'=','bank_loan_accounts.department_id')
            ->select(DB::raw('core_departments.name AS grp,SUM(amount) as amount,COUNT(bank_loan_accounts.id) as val'))
            ->groupBy('grp')
            ->where('bank_loan_accounts.initiated_date', '>=', $fdate)
            ->where('bank_loan_accounts.initiated_date', '<=', $tdate)
            ->where('bank_loan_accounts.status', '=', 'released')
            ->get();

            foreach($loanReleased as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.loan-released',compact('loanReleased','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "account-wise"){
            $loanReleased = DB::table('bank_loan_accounts')
            ->leftjoin('bank_accounts','bank_accounts.id' ,'=','bank_loan_accounts.account_id')
            ->select(DB::raw('bank_accounts.account_number AS grp,SUM(amount) as amount,COUNT(bank_loan_accounts.id) as val'))
            ->groupBy('grp')
            ->where('bank_loan_accounts.initiated_date', '>=', $fdate)
            ->where('bank_loan_accounts.initiated_date', '<=', $tdate)
            ->where('bank_loan_accounts.status', '=', 'released')
            ->get();

            foreach($loanReleased as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.loan-released',compact('loanReleased','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "customer-wise"){
            $loanReleased = DB::table('bank_loan_accounts')
            ->leftjoin('core_users','core_users.id' ,'=','bank_loan_accounts.requested_by')
            ->select(DB::raw('concat(core_users.title,".",core_users.first_name," ",core_users.last_name) as grp,SUM(amount) as amount,COUNT(bank_loan_accounts.id) as val'))
            ->groupBy('grp')
            ->where('bank_loan_accounts.initiated_date', '>=', $fdate)
            ->where('bank_loan_accounts.initiated_date', '<=', $tdate)
            ->where('bank_loan_accounts.status', '=', 'released')
            ->get();

            foreach($loanReleased  as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.loan-released',compact('loanReleased','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }
    }
}
