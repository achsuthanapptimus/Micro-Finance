<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;

class ReportHomeController extends Controller
{
    public function index(Request $request)
    {
        try {
            $fdate = $request->fdate;
            $tdate = $request->tdate;
    
            return view('pages.reports.home')
                        ->with("fdate", $fdate)
                        ->with("tdate", $tdate);

        } catch(Exception $e) {
            return redirect()->route('error');
        }
    }

    public function login(Request $request)
    {
        $url = config('app.front_url');


        if (!$request->has('token')) {
            return Redirect::to($url);
        }elseif ($request->token == '') {
            return Redirect::to($url);
        }
        $user = User::where('token', $request->token)->first();
        if ($user) {
            Auth::login($user);
            setPermissions();
            return redirect('/');
        }elseif(Auth::check()){
            $request->session()->put('did', $request->did);
            setPermissions();
            return redirect('/');
        }else{
            return Redirect::to($url);
        }


    }
}
