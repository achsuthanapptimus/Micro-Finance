<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SavingAccountClosed;
use DB;
use DateTime;

class SavingAccountClosedController extends Controller
{
    //
    public function index(Request $request)
    {

        $dt = new DateTime();
        $search="";
        $xaxisVals = array();
        $yaxisVals = array();



        if($request->get('fdate')){
            $fdate = $request->get('fdate');
        }else{
            $fdate = $dt->format('Y-m-d');
        }

        if($request->get('tdate')){
            $tdate = $request->get('tdate');
        }else{
            $tdate = $dt->format('Y-m-d');
        }

        $grouping=$request->grouping;
        
        if($request->grouping == "day-wise"){
             $savingAccountClosed = DB::table('bank_accounts')
            ->select(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d - %W") AS grp,COUNT(id) as val'))
            ->where('account_category','SAVINGS')
            ->Where('status', 'CLOSED')
            ->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d - %W")'))
            ->where('created_at', '>=', $fdate)
            ->where('created_at', '<=', $tdate)
            ->get();

            foreach($savingAccountClosed as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.saving-account-closed',compact('savingAccountClosed','grouping','fdate','tdate','xaxisVals','yaxisVals'));

        }
        elseif($request->grouping == "month-wise"){
            $savingAccountClosed  = DB::table('bank_accounts')
            ->select(DB::raw('DATE_FORMAT(created_at, "%Y-%m - %M") AS grp,COUNT(id) as val'))
            ->where('account_category','SAVINGS')
            ->Where('status', 'CLOSED')
            ->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y-%m - %M")'))
            ->where('created_at', '>=', $fdate)
            ->where('created_at', '<=', $tdate)
            ->get();

            foreach($savingAccountClosed as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.saving-account-closed',compact('savingAccountClosed','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "year-wise"){
            $savingAccountClosed = DB::table('bank_accounts')
            ->select(DB::raw('DATE_FORMAT(created_at, "%Y") AS grp,COUNT(id) as val'))
            ->where('account_category','SAVINGS')
            ->Where('status', 'CLOSED')
            ->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y")'))
            ->where('created_at', '>=', $fdate)
            ->where('created_at', '<=', $tdate)
            ->get();

            foreach($savingAccountClosed as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.saving-account-closed',compact('savingAccountClosed','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "department-wise"){
            $savingAccountClosed= DB::table('bank_accounts')
            ->leftjoin('core_departments','core_departments.id' ,'=','bank_accounts.department_id')
            ->select(DB::raw('core_departments.name AS grp,COUNT(bank_accounts.id) as val'))
            ->where('account_category','SAVINGS')
            ->Where('status', 'CLOSED')
            ->groupBy('grp')
            ->where('bank_accounts.created_at', '>=', $fdate)
            ->where('bank_accounts.created_at', '<=', $tdate)
            ->get();

            foreach($savingAccountClosed as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.saving-account-closed',compact('savingAccountClosed','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "account-wise"){
            $savingAccountClosed = DB::table('bank_accounts')
            ->select(DB::raw('bank_accounts.account_number AS grp,COUNT(bank_accounts.id) as val'))
            ->where('account_category','SAVINGS')
            ->Where('status', 'CLOSED')
            ->groupBy('grp')
            ->where('bank_accounts.created_at', '>=', $fdate)
            ->where('bank_accounts.created_at', '<=', $tdate)
            ->get();

            foreach($savingAccountClosed as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.saving-account-closed',compact('savingAccountClosed','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "customer-wise"){
            $savingAccountClosed = DB::table('bank_accounts')
            ->leftjoin('bank_account_holders','bank_account_holders.account_id' ,'=','bank_accounts.id')
            ->leftjoin('core_users','core_users.id' ,'=','bank_account_holders.user_id')
            ->select(DB::raw('concat(core_users.title,".",core_users.first_name," ",core_users.last_name) as grp,COUNT(bank_accounts.id) as val'))
            ->where('account_category','SAVINGS')
            ->Where('bank_accounts.status', 'CLOSED')
            ->groupBy('grp')
            ->where('bank_accounts.created_at', '>=', $fdate)
            ->where('bank_accounts.created_at', '<=', $tdate)
            ->get();

            foreach($savingAccountClosed as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.saving-account-closed',compact('savingAccountClosed','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }
}
}
