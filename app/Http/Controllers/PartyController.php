<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Party;
use App\Models\Address;
use App\Http\Controllers\Controller;
use App\Http\Requests\PartyStoreRequest;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class PartyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->is('api*')) {

            
            //$user = $request->is('api*') ? Auth::guard('api')->user() : Auth::user();
            $search = $request->search;
            $sortBy = $request->sortBy ? $request->sortBy : 'core_users.created_at';
            $sortDirection = $request->sortDirection ? $request->sortDirection : 'asc';
            $pagePer = $request->pagePer ? $request->pagePer  :Config::get('constant.pagination');
            $parties = Party::select('core_users.id','core_users.title','core_users.first_name','core_users.last_name','core_users.contact_primary','core_users.email','core_users.status',
                            'core_parties.type','core_users.id')               
                            ->leftjoin('core_users','core_parties.user_id','=','core_users.id')
                            ->where('core_users.first_name' ,'LIKE' ,"%$search%")
                            ->orWhere('core_users.last_name' ,'LIKE' ,"%$search%")
                            ->orWhere('core_users.contact_primary' ,'LIKE' ,"%$search%")
                            ->orWhere('core_users.identifier_value' ,'LIKE' ,"%$search%")
                            ->orWhere('core_users.email' ,'LIKE' ,"%$search%")
                            ->orderBy($sortBy,$sortDirection)
                            ->paginate($pagePer);
            
                return response()->json([
                    "success" => true,
                    "message" => "Operation successful.",
                    "data" => $parties,
              ],200);
        }

        $search = $request->search;
        $sortBy = $request->sortBy ? $request->sortBy : 'core_users.created_at';
        $sortDirection = $request->sortDirection ? $request->sortDirection : 'asc';
        $pagePer = $request->pagePer ? $request->pagePer  :Config::get('constant.pagination');
        $parties = Party::select('core_users.id','core_users.title','core_users.first_name','core_users.last_name','core_users.contact_primary','core_users.email','core_users.status',
                        'core_parties.type','core_users.id')               
                        ->leftjoin('core_users','core_parties.user_id','=','core_users.id')
                        ->where('core_users.department_id','=',Auth::user()->department_id)
                        ->where('core_users.first_name' ,'LIKE' ,"%$search%")
                        ->orWhere('core_users.last_name' ,'LIKE' ,"%$search%")
                        ->orWhere('core_users.contact_primary' ,'LIKE' ,"%$search%")
                        ->orWhere('core_users.identifier_value' ,'LIKE' ,"%$search%")
                        ->orWhere('core_users.email' ,'LIKE' ,"%$search%")
                        ->orderBy($sortBy,$sortDirection)
                        ->paginate($pagePer);
        $departments = Department::whereNull('parent_id')->get();


        return view('pages.party.index',compact('parties','search','sortBy','sortDirection','pagePer','departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.party.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PartyStoreRequest $request)
    {
        try{
            $user = User::create($request->validated());
            
            $address=new Address();
            $address->city=$request->city;
            $address->province=$request->province;
            $address->district=$request->district;
            $address->gsdivision=$request->gsdivision;
            $address->residential_address=$request->residential_address;
            $address->official_address=$request->official_address;
            $address->user_id=$user->id;
            $address->save();
            
            $party = new Party();
            $party->user_id = $user->id;   
            $party->type = $request->type;   
            $party->save();

            if ($request->is('api*')) {
                return response()->json([
                    "success" => true,
                    "message" => "Operation successful.",
                    "data" => $user,
                ],200);
            }

            return redirect()->route('parties.index');
        }catch (\Exception $th) {
            dd($th->getMessage());
            return redirect()->route('parties.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if ($request->is('api*')) {
            
            $user = User::find($id);
            $address = Address::where('core_addresses.user_id','=',$user->id)->first();
            $party = Party::where('core_parties.user_id','=',$user->id)->first();
            
            $responseData = compact('user','address','party');

            return response()->json([
                "success" => true,
                "message" => "Operation successful.",
                "data" => $responseData,
            ],200);
        }
        $user = User::find($id);
        $address = Address::where('core_addresses.user_id','=',$user->id)->first();
        $party = Party::where('core_parties.user_id','=',$user->id)->first();
        return view('pages.party.show',compact('user','address','party'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $address = Address::where('core_addresses.user_id','=',$user->id)->first();
        $party = Party::where('core_parties.user_id','=',$user->id)->first();
        return view('pages.party.edit',compact('user','address','party'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(PartyStoreRequest $request, $id)
    {
            try {

                if ($request->is('api*')) {

                    $user = User::find($id);
                    $user->title=$request->title;
                    $user->identifier_value=$request->identifier_value;
                    $user->first_name=$request->first_name;
                    $user->last_name=$request->last_name;
                    $user->middle_name=$request->middle_name;
                    $user->dob=$request->dob;
                    $user->dp=$request->dp;
                    $user->gender=$request->gender;
                    $user->maritial_status=$request->maritial_status;
                    $user->nationality=$request->nationality;
                    $user->contact_primary=$request->contact_primary;
                    $user->email=$request->email;
                    $user->save();
                    
                    $address = Address::where('core_addresses.user_id','=',$user->id)->first();
                    $address->city=$request->city;
                    $address->province=$request->province;
                    $address->district=$request->district;
                    $address->gsdivision=$request->gsdivision;
                    $address->residential_address=$request->residential_address;
                    $address->official_address=$request->official_address;
                    $address->user_id=$user->id;
                    $address->save();

                    $party = Party::where('core_parties.user_id','=',$user->id)->first();
                    $party->user_id = $user->id;   
                    $party->type = $request->type;   
                    $party->save();
                
                    
                    return response()->json([
                        "success" => true,
                        "message" => "Operation successful.",
                        "data" => $user,
                    ],200);
                }
                
                $user = User::find($id);
                $user->title=$request->title;
                $user->identifier_value=$request->identifier_value;
                $user->first_name=$request->first_name;
                $user->last_name=$request->last_name;
                $user->middle_name=$request->middle_name;
                $user->dob=$request->dob;
                $user->dp=$request->dp;
                $user->gender=$request->gender;
                $user->maritial_status=$request->maritial_status;
                $user->nationality=$request->nationality;
                $user->contact_primary=$request->contact_primary;
                $user->email=$request->email;
                $user->save();
                
                $address = Address::where('core_addresses.user_id','=',$user->id)->first();
                $address->city=$request->city;
                $address->province=$request->province;
                $address->district=$request->district;
                $address->gsdivision=$request->gsdivision;
                $address->residential_address=$request->residential_address;
                $address->official_address=$request->official_address;
                $address->user_id=$user->id;
                $address->save();

                $party = Party::where('core_parties.user_id','=',$user->id)->first();
                $party->user_id = $user->id;   
                $party->type = $request->type;   
                $party->save();

                return redirect()->route('parties.index');
            } catch (\Exception $th) {
                dd($th->getMessage());
                return redirect()->route('parties.create');
            }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
