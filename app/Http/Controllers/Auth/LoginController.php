<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use Auth,DB;
use App\Models\LoginHistory;
use GuzzleHttp\Client;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    protected $username;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->username = $this->findUsername();
    }

    public function findUsername()
    {
        $login = request()->input('username');

        $fieldType = 'username';
        request()->merge([$fieldType => $login]);

        return $fieldType;
    }

    public function username()
    {
        return $this->username;
    }

    public function login(Request $request)
    {
        $agent = new Agent();
        $platform = $agent->platform();
        $browser = $agent->browser();

        try{
            DB::beginTransaction();

            $credentials = $request->only('username', 'password');

            if (Auth::attempt($credentials)) {
                $user = Auth::user();

                if ($user->status == 'inactive') {

                    $loginHistory = new LoginHistory();
                    $loginHistory->device = $platform;
                    $loginHistory->browser = $browser;
                    $loginHistory->status = "failed";
                    $loginHistory->save();

                    Auth::logout();
                    return redirect('/login')->withErrors(['username' => 'This is user is inactive. Please contact your admin.']);
                }elseif(!$user->userRole->first()){

                    $loginHistory = new LoginHistory();
                    $loginHistory->device = $platform;
                    $loginHistory->browser = $browser;
                    $loginHistory->status = "failed";
                    $loginHistory->save();

                    Auth::logout();
                    return redirect('/login')->withErrors(['username' => 'This is user is inactive. Please contact your admin.']);
                }

                $loginHistory = new LoginHistory();
                $loginHistory->device = $platform;
                $loginHistory->browser = $browser;
                $loginHistory->status = "success";
                $loginHistory->save();
                
                $token = $user->createToken(env('CLIENT_NAME'))->accessToken;
                $user->token = $token;
                $user->save();
                
                DB::commit();
                return redirect()->route('home');
            }

            $loginHistory = new LoginHistory();
            $loginHistory->device = $platform;
            $loginHistory->browser = $browser;
            $loginHistory->status = "failed";
            $loginHistory->save();
            
            DB::commit();
            return redirect('/login')->withErrors(['username' => 'These credentials do not match our records.']);
        }catch (Exception $th) {
            DB::rollback();
            return redirect('/login')->withErrors(['username' => 'These credentials do not match our records.']);
        }

        

    }

    public function logout(Request $request) {
        $user = Auth::user();
        // DB::table('core_user_tokens')->where('users_id', $user->id)->delete();
        Auth::logout();
        $request->session()->invalidate();
        return redirect('/login');
    }
}
