<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;
use stdClass;

class LoanReportController extends Controller
{
    public function detailed(Request $request)
    {
        $fdate = $request->fdate;
        $status = $request->status;


        $query = "SELECT l.id, l.code, l.amount, DATE(l.created_at) created, CONCAT(cu.first_name, ' ', cu.last_name) customer, lp.title product, csu.name csu, dep.name department, l.status, l.amount, lf.fees fees, sf.amount AS sec_fund, lr.amount released, rent.amount rental, payable.amount payable, paid.amount paid
                    FROM bank_loan_accounts l
                    LEFT JOIN bank_accounts a ON a.id = l.account_id
                    LEFT JOIN (SELECT account_id, MAX(user_id) user_id FROM `bank_account_holders` GROUP BY account_id) ah ON ah.account_id = a.id
                    LEFT JOIN `core_users` cu ON ah.user_id = cu.id
                    LEFT JOIN `bank_loan_products` lp ON lp.id = l.loan_product_id
                    LEFT JOIN `bank_csus` csu ON csu.id = l.csu_id
                    LEFT JOIN `core_departments` dep ON dep.id = l.department_id
                    LEFT JOIN (
                        SELECT f.account_id, SUM(IFNULL(t.amount,0)) fees
                        FROM `bank_fees` f LEFT JOIN `bank_account_transactions` t ON t.id = f.account_transaction_id
                        GROUP BY f.account_id
                    ) lf ON a.id = lf.account_id
                    LEFT JOIN (
                        SELECT linked_loan_account_id account_id, SUM(IF(`type`='IN',amount,0-amount)) amount FROM `bank_security_fund_transactions` GROUP BY `linked_loan_account_id`
                    ) sf ON sf.account_id = l.id
                    LEFT JOIN (
                        SELECT linked_loan_account_id account_id, SUM(released_amount) amount FROM `bank_loan_releases` GROUP BY  linked_loan_account_id
                    ) lr ON lr.account_id = l.id
                    LEFT JOIN (
                        SELECT `loan_account_id`, MAX(`payable_capital`+`payable_interest`) amount FROM `bank_repayment_schedules` GROUP BY loan_account_id
                    ) rent ON rent.loan_account_id = l.id
                    LEFT JOIN (
                        SELECT loan_account_id account_id, SUM(payable_capital + payable_interest) amount FROM `bank_repayment_schedules` GROUP BY  loan_account_id
                    ) payable ON payable.account_id = l.id
                    LEFT JOIN (
                        SELECT loan_account_id account_id, SUM(collected_capital + collected_interest) amount FROM `bank_repayments` GROUP BY  loan_account_id
                    ) paid ON paid.account_id = l.id";


        $data = DB::select($query);

        //Filter data
        $filterData = new stdClass();
        $filterData->departments = DB::select("SELECT * from core_departments order by name");
        $filterData->status = DB::select("SELECT DISTINCT(status) as status from bank_loan_accounts order by status");

        //Selected filters
        $selectedFilters = new stdClass();
        $selectedFilters->fdate = $fdate;
        $selectedFilters->status = $status;

        return view('pages.reports.loan-report-detailed', ['data' => $data, 'print' => $request->print, 'filterData' => $filterData, 'selectedFilters' => $selectedFilters]);
    }

    public function summary(Request $request)
    {
        $type = $request->type;
        $data = new stdClass();
        $type = $type == "" ? "Product" : $type;

        $baseQuery = "FROM bank_loan_accounts l
                        LEFT JOIN bank_accounts a ON a.id = l.account_id
                        LEFT JOIN (SELECT account_id, MAX(user_id) user_id FROM `bank_account_holders` GROUP BY account_id) ah ON ah.account_id = a.id
                        LEFT JOIN `core_users` cu ON ah.user_id = cu.id
                        LEFT JOIN `bank_loan_products` lp ON lp.id = l.loan_product_id
                        LEFT JOIN `bank_csus` csu ON csu.id = l.csu_id
                        LEFT JOIN `core_departments` dep ON dep.id = l.department_id
                        LEFT JOIN (
                            SELECT f.account_id, SUM(IFNULL(t.amount,0)) fees
                            FROM `bank_fees` f LEFT JOIN `bank_account_transactions` t ON t.id = f.account_transaction_id
                            GROUP BY f.account_id
                        ) lf ON a.id = lf.account_id
                        LEFT JOIN (
                            SELECT linked_loan_account_id account_id, SUM(IF(`type`='IN',amount,0-amount)) amount FROM `bank_security_fund_transactions` GROUP BY `linked_loan_account_id`
                        ) sf ON sf.account_id = l.id
                        LEFT JOIN (
                            SELECT loan_account_id account_id, SUM(payable_capital + payable_interest) amount FROM `bank_repayment_schedules` GROUP BY  loan_account_id
                        ) payable ON payable.account_id = l.id
                        LEFT JOIN (
                            SELECT loan_account_id account_id, SUM(collected_capital + collected_interest) amount FROM `bank_repayments` GROUP BY  loan_account_id
                        ) paid ON paid.account_id = l.id";

        if($type == "Product" || $type == "All")
        {
            $data->Product = DB::select("SELECT lp.id, lp.title item, COUNT(*) cnt, SUM(l.amount) amount, SUM(payable.amount) payable, SUM(lf.fees) fees, SUM(sf.amount) AS secfund, SUM(paid.amount) paid
                                ". $baseQuery . " 
                             GROUP BY lp.id, lp.title");
        }
        
        if($type == "CSU" || $type == "All")
        {
            $data->CSU = DB::select("SELECT csu.id, csu.name item, COUNT(*) cnt, SUM(l.amount) amount, SUM(payable.amount) payable, SUM(lf.fees) fees, SUM(sf.amount) AS secfund, SUM(paid.amount) paid
                                ". $baseQuery . " 
                             GROUP BY csu.id, csu.name");
        }
        
        if($type == "Branch" || $type == "All")
        {
            $data->Branch = DB::select("SELECT dep.id, dep.name item, COUNT(*) cnt, SUM(l.amount) amount, SUM(payable.amount) payable, SUM(lf.fees) fees, SUM(sf.amount) AS secfund, SUM(paid.amount) paid
                                ". $baseQuery . " 
                             GROUP BY dep.id, dep.name");
        }

        return view('pages.reports.loan-report-summary', ['data' => $data, 'type' => $type, 'print' => $request->print]);
    }

    public function combinedSummary(Request $request)
    {
        $type = $request->type;
        $data = null;
        $type = $type == "" ? "Product" : $type;

        $baseQuery = "FROM bank_loan_accounts l
                        LEFT JOIN bank_accounts a ON a.id = l.account_id
                        LEFT JOIN (SELECT account_id, MAX(user_id) user_id FROM `bank_account_holders` GROUP BY account_id) ah ON ah.account_id = a.id
                        LEFT JOIN `core_users` cu ON ah.user_id = cu.id
                        LEFT JOIN `bank_loan_products` lp ON lp.id = l.loan_product_id
                        LEFT JOIN `bank_csus` csu ON csu.id = l.csu_id
                        LEFT JOIN `core_users` cum ON csu.manager = cum.id
                        LEFT JOIN `core_departments` dep ON dep.id = l.department_id
                        LEFT JOIN (
                            SELECT loan_account_id account_id, SUM(payable_capital + payable_interest) amount FROM `bank_repayment_schedules` GROUP BY  loan_account_id
                        ) payable ON payable.account_id = l.id
                        LEFT JOIN (
                            SELECT loan_account_id account_id, SUM(collected_capital + collected_interest) amount FROM `bank_repayments` GROUP BY  loan_account_id
                        ) paid ON paid.account_id = l.id";

                        $data = DB::select("SELECT dep.id, csu.id, lp.id, dep.name branch, csu.name csu, cum.first_name cofficer, lp.title product,  COUNT(*) cnt, SUM(l.amount) amount, SUM(payable.amount) payable, SUM(paid.amount) paid
                        ". $baseQuery . " 
                     GROUP BY dep.id, csu.id, lp.id");

        return view('pages.reports.loan-report-combined-summary', ['data' => $data, 'type' => $type, 'print' => $request->print]);
    }

    public function release(Request $request)
    {
        $data = DB::select("SELECT l.id, l.code, l.amount loanamount, CONCAT(cu.first_name, ' ', cu.last_name) customer, lp.title product, CONCAT(co.first_name, ' ', co.last_name) officer, csu.name csu, dep.name department, lr.released_amount ramount, DATE(lr.released_at) released_at,  lf.fees fees, sf.amount AS secfund,rent.amount rental, interest.amount interest, payable.amount payable
                            FROM bank_loan_releases lr
                            LEFT JOIN bank_loan_accounts l ON l.id = lr.linked_loan_account_id
                            LEFT JOIN bank_accounts a ON a.id = l.account_id
                            LEFT JOIN (
                                SELECT `loan_account_id`, MAX(`payable_capital`+`payable_interest`) amount FROM `bank_repayment_schedules` GROUP BY loan_account_id
                            ) rent ON rent.loan_account_id = l.id
                            LEFT JOIN (
                                SELECT loan_account_id account_id, SUM(payable_capital + payable_interest) amount FROM `bank_repayment_schedules` GROUP BY  loan_account_id
                            ) payable ON payable.account_id = l.id
                            LEFT JOIN (
                                SELECT loan_account_id account_id, SUM(payable_interest) amount FROM `bank_repayment_schedules` GROUP BY  loan_account_id
                            ) interest ON interest.account_id = l.id
                            LEFT JOIN (
                                SELECT f.account_id, SUM(IFNULL(t.amount,0)) fees
                                FROM `bank_fees` f LEFT JOIN `bank_account_transactions` t ON t.id = f.account_transaction_id
                                GROUP BY f.account_id
                            ) lf ON a.id = lf.account_id
                            LEFT JOIN (
                                SELECT linked_loan_account_id account_id, SUM(IF(`type`='IN',amount,0-amount)) amount FROM `bank_security_fund_transactions` GROUP BY `linked_loan_account_id`
                            ) sf ON sf.account_id = l.id
                            LEFT JOIN (SELECT account_id, MAX(user_id) user_id FROM `bank_account_holders` GROUP BY account_id) ah ON ah.account_id = a.id
                            LEFT JOIN `core_users` cu ON ah.user_id = cu.id
                            LEFT JOIN `bank_loan_products` lp ON lp.id = l.loan_product_id
                            LEFT JOIN `bank_csus` csu ON csu.id = l.csu_id
                            LEFT JOIN `core_departments` dep ON dep.id = l.department_id
                            LEFT JOIN `core_users` co ON lr.released_by = co.id");

        return view('pages.reports.release-report-detailed', ['data' => $data, 'print' => $request->print]);
    }

    public function releaseSummary(Request $request)
    {
        $baseQuery = "FROM bank_loan_releases lr
        LEFT JOIN bank_loan_accounts l ON l.id = lr.linked_loan_account_id
        LEFT JOIN bank_accounts a ON a.id = l.account_id
        LEFT JOIN `bank_loan_products` lp ON lp.id = l.loan_product_id
        LEFT JOIN `core_departments` dep ON dep.id = l.department_id
        LEFT JOIN `core_users` co ON lr.released_by = co.id";

        $selectQuery = "";
        $groupQuery = "";
        $type = $request->type;
        $type = $type == "" ? "Branch" : $type;
        $data = new stdClass();

        if($type == "Officer" || $type == "All")
        {
            $selectQuery = "SELECT CONCAT(co.first_name, ' ', co.last_name) item, SUM(lr.released_amount) ramount ";
            $groupQuery = " GROUP BY co.id ";
            $data->Officer = DB::select($selectQuery . ", COUNT(*) cnt " .$baseQuery. " ".$groupQuery);
        }
        
        if($type == "Product" || $type == "All")
        {
            $selectQuery = "SELECT lp.title item, SUM(lr.released_amount) ramount ";
            $groupQuery = " GROUP BY lp.id ";
            $data->Product = DB::select($selectQuery . ", COUNT(*) cnt " .$baseQuery. " ".$groupQuery);
        }
        
        if($type == "Branch" || $type == "All")
        {
            $selectQuery = "SELECT dep.name item, SUM(lr.released_amount) ramount ";
            $groupQuery = " GROUP BY dep.id ";
            $data->Branch = DB::select($selectQuery . ", COUNT(*) cnt " .$baseQuery. " ".$groupQuery);
        }

        return view('pages.reports.release-report-summary', ['data' => $data, 'type' => $type, 'print' => $request->print]);
    }

    public function collectionDetailed(Request $request)
    {
        $data = $this::getCollectionData();
        return view('pages.reports.collection-report-detailed', ['data' => $data, 'print' => $request->print]);
    }

    public function getCollectionData()
    {
        $data = DB::select("SELECT l.id, l.code, l.amount, CONCAT(cu.first_name, ' ', cu.last_name) customer, lp.title product, CONCAT(co.first_name, ' ', co.last_name) officer, csu.name csu, dep.name department, l.amount, (rp.collected_capital + rp.collected_interest) paid 
                            FROM bank_repayments rp
                            LEFT JOIN bank_loan_accounts l ON l.id = rp.loan_account_id
                            LEFT JOIN bank_accounts a ON a.id = l.account_id
                            LEFT JOIN (SELECT account_id, MAX(user_id) user_id FROM `bank_account_holders` GROUP BY account_id) ah ON ah.account_id = a.id
                            LEFT JOIN `core_users` cu ON ah.user_id = cu.id
                            LEFT JOIN `bank_loan_products` lp ON lp.id = l.loan_product_id
                            LEFT JOIN `bank_csus` csu ON csu.id = l.csu_id
                            LEFT JOIN `core_departments` dep ON dep.id = l.department_id
                            LEFT JOIN `core_users` co ON rp.collected_by = co.id");
        return $data;
    }

    public function collectionSummary(Request $request)
    {
        $type = $request->type;
        $data = new stdClass();
        $type = $type == "" ? "Product" : $type;

        $baseQuery = "FROM bank_loan_accounts l
                        LEFT JOIN bank_accounts a ON a.id = l.account_id
                        LEFT JOIN (SELECT account_id, MAX(user_id) user_id FROM `bank_account_holders` GROUP BY account_id) ah ON ah.account_id = a.id
                        LEFT JOIN `core_users` cu ON ah.user_id = cu.id
                        LEFT JOIN `bank_loan_products` lp ON lp.id = l.loan_product_id
                        LEFT JOIN `bank_csus` csu ON csu.id = l.csu_id
                        LEFT JOIN `core_departments` dep ON dep.id = l.department_id
                        LEFT JOIN (
                            SELECT loan_account_id account_id, SUM(payable_capital + payable_interest) amount FROM `bank_repayment_schedules` GROUP BY  loan_account_id
                        ) payable ON payable.account_id = l.id
                        LEFT JOIN (
                            SELECT loan_account_id account_id, SUM(collected_capital + collected_interest) amount FROM `bank_repayments` GROUP BY  loan_account_id
                        ) paid ON paid.account_id = l.id";

        if($type == "Product" || $type == "All")
        {
            $data->Product = DB::select("SELECT lp.id, lp.title item, COUNT(*) cnt, SUM(l.amount) amount, SUM(payable.amount) payable, SUM(paid.amount) paid
                                ". $baseQuery . " 
                             GROUP BY lp.id");
        }
        
        if($type == "CSU" || $type == "All")
        {
            $data->CSU = DB::select("SELECT csu.id, csu.name item, COUNT(*) cnt, SUM(l.amount) amount, SUM(payable.amount) payable, SUM(paid.amount) paid
                                ". $baseQuery . " 
                             GROUP BY csu.id");
        }
        
        if($type == "Branch" || $type == "All")
        {
            $data->Branch = DB::select("SELECT dep.id, dep.name item, COUNT(*) cnt, SUM(l.amount) amount, SUM(payable.amount) payable, SUM(paid.amount) paid
                                ". $baseQuery . " 
                             GROUP BY dep.id");
        }
        
        if($type == "Officer" || $type == "All")
        {
            $data->Officer = DB::select("SELECT cu.id, cu.first_name item, COUNT(*) cnt, SUM(l.amount) amount, SUM(payable.amount) payable, SUM(paid.amount) paid
                                ". $baseQuery . " 
                             GROUP BY cu.id");
        }
        
        if($type == "Customer" || $type == "All")
        {
            $data->Customer = DB::select("SELECT cu.id, cu.first_name item, COUNT(*) cnt, SUM(l.amount) amount, SUM(payable.amount) payable, SUM(paid.amount) paid
                                ". $baseQuery . " 
                             GROUP BY cu.id");
        }

        //detailed data for printing
        $detailedData = null;
        if($type == "All")
        {
            $detailedData = $this::getCollectionData();
        }

        return view('pages.reports.collection-report-summary', ['data' => $data, 'detailedData' => $detailedData, 'type' => $type, 'print' => $request->print]);
    }


    public function npl(Request $request)
    {
        $data = DB::select("SELECT l.id, l.code, l.amount, DATE(l.created_at) created, CONCAT(cu.first_name, ' ', cu.last_name) customer, lp.title product, csu.name csu, dep.name department, l.status, l.amount, lf.fees fees, sf.amount AS sec_fund, lr.amount released, rent.amount rental, payable.amount payable, paid.amount paid
                            FROM bank_loan_accounts l
                            LEFT JOIN bank_accounts a ON a.id = l.account_id
                            LEFT JOIN (SELECT account_id, MAX(user_id) user_id FROM `bank_account_holders` GROUP BY account_id) ah ON ah.account_id = a.id
                            LEFT JOIN `core_users` cu ON ah.user_id = cu.id
                            LEFT JOIN `bank_loan_products` lp ON lp.id = l.loan_product_id
                            LEFT JOIN `bank_csus` csu ON csu.id = l.csu_id
                            LEFT JOIN `core_departments` dep ON dep.id = l.department_id
                            LEFT JOIN (
                                SELECT f.account_id, SUM(IFNULL(t.amount,0)) fees
                                FROM `bank_fees` f LEFT JOIN `bank_account_transactions` t ON t.id = f.account_transaction_id
                                GROUP BY f.account_id
                            ) lf ON a.id = lf.account_id
                            LEFT JOIN (
                                SELECT linked_loan_account_id account_id, SUM(IF(`type`='IN',amount,0-amount)) amount FROM `bank_security_fund_transactions` GROUP BY `linked_loan_account_id`
                            ) sf ON sf.account_id = l.id
                            LEFT JOIN (
                                SELECT linked_loan_account_id account_id, SUM(released_amount) amount FROM `bank_loan_releases` GROUP BY  linked_loan_account_id
                            ) lr ON lr.account_id = l.id
                            LEFT JOIN (
                                SELECT `loan_account_id`, MAX(`payable_capital`+`payable_interest`) amount FROM `bank_repayment_schedules` GROUP BY loan_account_id
                            ) rent ON rent.loan_account_id = l.id
                            LEFT JOIN (
                                SELECT loan_account_id account_id, SUM(payable_capital + payable_interest) amount FROM `bank_repayment_schedules` GROUP BY  loan_account_id
                            ) payable ON payable.account_id = l.id
                            LEFT JOIN (
                                SELECT loan_account_id account_id, SUM(collected_capital + collected_interest) amount FROM `bank_repayments` GROUP BY  loan_account_id
                            ) paid ON paid.account_id = l.id WHERE is_npl = true");

        return view('pages.reports.loan-report-npl', ['data' => $data, 'print' => $request->print]);
    }

    public function fees(Request $request)
    {
        $data = DB::select("SELECT DATE(lf.created_at) created, lp.title product, dep.name department, count(*) cnt, SUM(IFNULL(lf.fees,0)) fees, SUM(IFNULL(sf.amount,0)) AS sec_fund, SUM(IFNULL(insurance.amount,0)) insurance
                            FROM (
                                SELECT f.account_id, IFNULL(t.amount,0) fees, f.created_at
                                FROM `bank_fees` f LEFT JOIN `bank_account_transactions` t ON t.id = f.account_transaction_id
                            ) lf 
                            LEFT JOIN bank_accounts a ON a.id = lf.account_id
                            LEFT JOIN bank_loan_accounts l ON a.id = l.account_id
                            LEFT JOIN `bank_loan_products` lp ON lp.id = l.loan_product_id
                            LEFT JOIN `core_departments` dep ON dep.id = l.department_id
                            LEFT JOIN (
                                SELECT linked_loan_account_id account_id, SUM(IF(`type`='IN',amount,0-amount)) amount FROM `bank_security_fund_transactions` GROUP BY `linked_loan_account_id`
                            ) sf ON sf.account_id = l.id
                            LEFT JOIN (
                                SELECT linked_loan_account_id account_id, SUM(amount) amount FROM `bank_insurances` GROUP BY `linked_loan_account_id`
                            ) insurance ON insurance.account_id = l.id GROUP BY DATE(lf.created_at), lp.id, dep.id");

        return view('pages.reports.fees-report', ['data' => $data, 'print' => $request->print]);
    }

    public function debtorBalance(Request $request)
    {
        $data = DB::select("SELECT u.id, CONCAT(first_name, ' ', last_name) cname, `contact_primary`, `contact_secondary`, CONCAT(identifier, ': ', identifier_value) identifier, (IFNULL(payable.amount,0) - IFNULL(paid.amount,0)) balance 
                            FROM core_users u
                            LEFT JOIN (
                                SELECT ah.user_id, SUM(payable_capital + payable_interest) amount FROM `bank_repayment_schedules` p
                                LEFT JOIN bank_loan_accounts l ON l.id = p.loan_account_id
                                LEFT JOIN bank_accounts a ON a.id = l.account_id
                                LEFT JOIN (SELECT account_id, MAX(user_id) user_id FROM `bank_account_holders` GROUP BY account_id) ah ON ah.account_id = a.id
                                WHERE p.deleted_at IS NULL
                                GROUP BY ah.user_id
                            ) payable ON u.id = payable.user_id
                            LEFT JOIN (
                                SELECT ah.user_id, SUM(collected_capital + collected_interest) amount FROM `bank_repayments` p
                                LEFT JOIN bank_loan_accounts l ON l.id = p.loan_account_id
                                LEFT JOIN bank_accounts a ON a.id = l.account_id
                                LEFT JOIN (SELECT account_id, MAX(user_id) user_id FROM `bank_account_holders` GROUP BY account_id) ah ON ah.account_id = a.id
                                WHERE p.deleted_at IS NULL
                                GROUP BY ah.user_id
                            ) paid ON u.id = paid.user_id
                            HAVING balance <> 0");

        return view('pages.reports.debtor-balance', ['data' => $data, 'print' => $request->print]);
    }

    public function productAnalysis(Request $request)
    {
        $data = DB::select("SELECT *
                            FROM core_users u");

        return view('pages.reports.analysis-product', ['data' => $data, 'print' => $request->print]);
    }

    public function csuAnalysis(Request $request)
    {
        $data = DB::select("SELECT *
                            FROM core_users u");

        return view('pages.reports.analysis-csu', ['data' => $data, 'print' => $request->print]);
    }

    public function branchAnalysis(Request $request)
    {
        $data = DB::select("SELECT *
                            FROM core_users u");

        return view('pages.reports.analysis-branch', ['data' => $data, 'print' => $request->print]);
    }
}
