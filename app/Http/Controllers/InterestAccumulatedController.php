<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;

class InterestAccumulatedController extends Controller
{
    public function index(Request $request)
    {

        $dt = new DateTime();
        $search="";
        $xaxisVals = array();
        $yaxisVals = array();



        if($request->get('fdate')){
            $fdate = $request->get('fdate');
        }else{
            $fdate = $dt->format('Y-m-d');
        }

        if($request->get('tdate')){
            $tdate = $request->get('tdate');
        }else{
            $tdate = $dt->format('Y-m-d');
        }

        $grouping=$request->grouping;
        
        if($request->grouping == "day-wise"){
             $loanAccumulated = DB::table('bank_loan_accounts')
            ->select(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d - %W") AS grp,interest_rate as val'))
            ->groupBy('grp','val')
            ->where('created_at', '>=', $fdate)
            ->where('created_at', '<=', $tdate)
            ->get();

            foreach($loanAccumulated as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.loan-accumulated',compact('loanAccumulated','grouping','fdate','tdate','xaxisVals','yaxisVals'));

        }
        elseif($request->grouping == "month-wise"){
            $loanAccumulated = DB::table('bank_loan_accounts')
            ->select(DB::raw('DATE_FORMAT(created_at, "%Y-%m - %M") AS grp,interest_rate as val'))
            ->groupBy('grp','val')
            ->where('created_at', '>=', $fdate)
            ->where('created_at', '<=', $tdate)
            ->get();

            foreach($loanAccumulated as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.loan-accumulated',compact('loanAccumulated','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "year-wise"){
            $loanAccumulated = DB::table('bank_loan_accounts')
            ->select(DB::raw('DATE_FORMAT(created_at, "%Y") AS grp,interest_rate as val'))
            ->groupBy('grp','val')
            ->where('created_at', '>=', $fdate)
            ->where('created_at', '<=', $tdate)
            ->get();

            foreach($loanAccumulated as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.loan-accumulated',compact('loanAccumulated','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "department-wise"){
            $loanAccumulated = DB::table('bank_loan_accounts')
            ->leftjoin('core_departments','core_departments.id' ,'=','bank_loan_accounts.department_id')
            ->select(DB::raw('core_departments.name AS grp,interest_rate as val'))
            ->groupBy('grp','val')
            ->where('bank_loan_accounts.created_at', '>=', $fdate)
            ->where('bank_loan_accounts.created_at', '<=', $tdate)
            ->get();

            foreach($loanAccumulated as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.loan-accumulated',compact('loanAccumulated','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "account-wise"){
            $loanAccumulated = DB::table('bank_loan_accounts')
            ->leftjoin('bank_accounts','bank_accounts.id' ,'=','bank_loan_accounts.account_id')
            ->select(DB::raw('bank_accounts.account_number AS grp,interest_rate as val'))
            ->groupBy('grp','val')
            ->where('bank_loan_accounts.created_at', '>=', $fdate)
            ->where('bank_loan_accounts.created_at', '<=', $tdate)
            ->get();

            foreach($loanAccumulated as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.loan-accumulated',compact('loanAccumulated','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "customer-wise"){
            $loanAccumulated = DB::table('bank_loan_accounts')
            ->leftjoin('core_users','core_users.id' ,'=','bank_loan_accounts.requested_by')
            ->select(DB::raw('concat(core_users.title,".",core_users.first_name," ",core_users.last_name) as grp,interest_rate as val'))
            ->groupBy('grp','val')
            ->where('bank_loan_accounts.created_at', '>=', $fdate)
            ->where('bank_loan_accounts.created_at', '<=', $tdate)
            ->get();

            foreach($loanAccumulated as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.loan-accumulated',compact('loanAccumulated','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }
    }
}
