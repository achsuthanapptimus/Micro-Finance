<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;

class WithdrawalController extends Controller
{
    public function index(Request $request)
    {

        $dt = new DateTime();
        $search="";
        $xaxisVals = array();
        $yaxisVals = array();



        if($request->get('fdate')){
            $fdate = $request->get('fdate');
        }else{
            $fdate = $dt->format('Y-m-d');
        }

        if($request->get('tdate')){
            $tdate = $request->get('tdate');
        }else{
            $tdate = $dt->format('Y-m-d');
        }

        $grouping=$request->grouping;
        
        if($request->grouping == "day-wise"){
             $bankWithdrawals = DB::table('bank_withdrawals')
             ->leftjoin('bank_account_transactions','bank_account_transactions.id' ,'=','bank_withdrawals.account_transaction_id')
            ->select(DB::raw('DATE_FORMAT(bank_withdrawals.created_at, "%Y-%m-%d - %W") AS grp,SUM(bank_account_transactions.amount) as amount,COUNT(bank_withdrawals.id) as val'))
            ->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d - %W")'))
            ->where('bank_withdrawals.created_at', '>=', $fdate)
            ->where('bank_withdrawals.created_at', '<=', $tdate)
            ->get();

            foreach($bankWithdrawals as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.withdrawal',compact('bankWithdrawals','grouping','fdate','tdate','xaxisVals','yaxisVals'));

        }
        elseif($request->grouping == "month-wise"){
            $bankWithdrawals = DB::table('bank_withdrawals')
            ->leftjoin('bank_account_transactions','bank_account_transactions.id' ,'=','bank_withdrawals.account_transaction_id')
           ->select(DB::raw('DATE_FORMAT(bank_withdrawals.created_at, "%Y-%m - %M") AS grp,SUM(bank_account_transactions.amount) as amount,COUNT(bank_withdrawals.id) as val'))
           ->groupBy('grp')
           ->where('bank_withdrawals.created_at', '>=', $fdate)
           ->where('bank_withdrawals.created_at', '<=', $tdate)
           ->get();

           foreach($bankWithdrawals as $r){
            array_push($xaxisVals, $r->grp);
            array_push($yaxisVals, $r->val);
        }

           return view('pages.reports.withdrawal',compact('bankWithdrawals','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "year-wise"){
            $bankWithdrawals = DB::table('bank_withdrawals')
            ->leftjoin('bank_account_transactions','bank_account_transactions.id' ,'=','bank_withdrawals.account_transaction_id')
           ->select(DB::raw('DATE_FORMAT(bank_withdrawals.created_at, "%Y") AS grp,SUM(bank_account_transactions.amount) as amount,COUNT(bank_withdrawals.id) as val'))
           ->groupBy('grp')
           ->where('bank_withdrawals.created_at', '>=', $fdate)
           ->where('bank_withdrawals.created_at', '<=', $tdate)
           ->get();

           foreach($bankWithdrawals as $r){
            array_push($xaxisVals, $r->grp);
            array_push($yaxisVals, $r->val);
        }

           return view('pages.reports.withdrawal',compact('bankWithdrawals','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "department-wise"){
            $bankWithdrawals = DB::table('bank_withdrawals')
            ->leftjoin('bank_account_transactions','bank_account_transactions.id' ,'=','bank_withdrawals.account_transaction_id')
            ->leftjoin('core_departments','core_departments.id' ,'=','bank_withdrawals.department_id')
            ->select(DB::raw('core_departments.name AS grp,SUM(bank_account_transactions.amount) as amount,COUNT(bank_withdrawals.id) as val'))
            ->groupBy('grp')
            ->where('bank_withdrawals.created_at', '>=', $fdate)
            ->where('bank_withdrawals.created_at', '<=', $tdate)
            ->get();

            foreach($bankWithdrawals as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.withdrawal',compact('bankWithdrawals','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "account-wise"){
            $bankWithdrawals = DB::table('bank_withdrawals')
            ->leftjoin('bank_account_transactions','bank_account_transactions.id' ,'=','bank_withdrawals.account_transaction_id')
            ->leftjoin('bank_accounts','bank_accounts.id' ,'=','bank_account_transactions.account_id')
            ->select(DB::raw('bank_accounts.account_number AS grp,SUM(bank_account_transactions.amount) as amount,COUNT(bank_withdrawals.id) as val'))
            ->groupBy('grp')
            ->where('bank_withdrawals.created_at', '>=', $fdate)
            ->where('bank_withdrawals.created_at', '<=', $tdate)
            ->get();

            foreach($bankWithdrawals as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.withdrawal',compact('bankWithdrawals','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "customer-wise"){
            $bankWithdrawals = DB::table('bank_withdrawals')
            ->leftjoin('bank_account_transactions','bank_account_transactions.id' ,'=','bank_withdrawals.account_transaction_id')
            ->leftjoin('bank_account_holders','bank_account_holders.account_id' ,'=','bank_account_transactions.account_id')
            ->leftjoin('core_users','core_users.id' ,'=','bank_account_holders.user_id')
            ->select(DB::raw('concat(core_users.title,".",core_users.first_name," ",core_users.last_name) as grp,SUM(bank_account_transactions.amount) as amount,COUNT(bank_withdrawals.id) as val'))
            ->groupBy('grp')
            ->where('bank_withdrawals.created_at', '>=', $fdate)
            ->where('bank_withdrawals.created_at', '<=', $tdate)
            ->get();

            foreach($bankWithdrawals as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.withdrawal',compact('bankWithdrawals','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }
    }
}
