<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;

class RepaidAmountController extends Controller
{
    //
    public function index(Request $request)
    {

        $dt = new DateTime();
        $search="";
        $xaxisVals = array();
        $yaxisVals = array();



        if($request->get('fdate')){
            $fdate = $request->get('fdate');
        }else{
            $fdate = $dt->format('Y-m-d');
        }

        if($request->get('tdate')){
            $tdate = $request->get('tdate');
        }else{
            $tdate = $dt->format('Y-m-d');
        }

        $grouping=$request->grouping;
        
        if($request->grouping == "day-wise"){
             $repaidAmount = DB::table('bank_account_transactions')
            ->select(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d - %W") AS grp,SUM(amount) as amount,COUNT(id) as val'))
            ->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d - %W")'))
            ->where('created_at', '>=', $fdate)
            ->where('created_at', '<=', $tdate)
            ->where('type', '=', 'LOAN_REPAYMENT')
            ->get();

            foreach($repaidAmount as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.repaid-amount',compact('repaidAmount','grouping','fdate','tdate','xaxisVals','yaxisVals'));

        }
        elseif($request->grouping == "month-wise"){
            $repaidAmount = DB::table('bank_account_transactions')
            ->select(DB::raw('DATE_FORMAT(created_at, "%Y-%m - %M") AS grp,SUM(amount) as amount,COUNT(id) as val'))
            ->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y-%m - %M")'))
            ->where('created_at', '>=', $fdate)
            ->where('created_at', '<=', $tdate)
            ->where('type', '=', 'LOAN_REPAYMENT')
            ->get();

            foreach($repaidAmount as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.repaid-amount',compact('repaidAmount','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "year-wise"){
            $repaidAmount = DB::table('bank_account_transactions')
            ->select(DB::raw('DATE_FORMAT(created_at, "%Y") AS grp,SUM(amount) as amount,COUNT(id) as val'))
            ->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y")'))
            ->where('created_at', '>=', $fdate)
            ->where('created_at', '<=', $tdate)
            ->where('type', '=', 'LOAN_REPAYMENT')
            ->get();

            foreach($repaidAmount as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.repaid-amount',compact('repaidAmount','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "department-wise"){
            $repaidAmount = DB::table('bank_account_transactions')
            ->leftjoin('core_departments','core_departments.id' ,'=','bank_account_transactions.department_id')
            ->select(DB::raw('core_departments.name AS grp,SUM(amount) as amount,COUNT(bank_account_transactions.id) as val'))
            ->groupBy('grp')
            ->where('bank_account_transactions.created_at', '>=', $fdate)
            ->where('bank_account_transactions.created_at', '<=', $tdate)
            ->where('bank_account_transactions.type', '=', 'LOAN_REPAYMENT')
            ->get();

            foreach($repaidAmount as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.repaid-amount',compact('repaidAmount','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "account-wise"){
            $repaidAmount = DB::table('bank_account_transactions')
            ->leftjoin('bank_accounts','bank_accounts.id' ,'=','bank_account_transactions.account_id')
            ->select(DB::raw('bank_accounts.account_number AS grp,SUM(amount) as amount,COUNT(bank_account_transactions.id) as val'))
            ->groupBy('grp')
            ->where('bank_account_transactions.created_at', '>=', $fdate)
            ->where('bank_account_transactions.created_at', '<=', $tdate)
            ->where('bank_account_transactions.type', '=', 'LOAN_REPAYMENT')
            ->get();

            foreach($repaidAmount as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.repaid-amount',compact('repaidAmount','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "customer-wise"){
            $repaidAmount = DB::table('bank_account_transactions')
            ->leftjoin('bank_account_holders','bank_account_holders.account_id' ,'=','bank_account_transactions.account_id')
            ->leftjoin('core_users','core_users.id' ,'=','bank_account_holders.user_id')
            ->select(DB::raw('concat(core_users.title,".",core_users.first_name," ",core_users.last_name) as grp,SUM(amount) as amount,COUNT(bank_account_transactions.id) as val'))
            ->groupBy('grp')
            ->where('bank_account_transactions.created_at', '>=', $fdate)
            ->where('bank_account_transactions.created_at', '<=', $tdate)
            ->where('bank_account_transactions.type', '=', 'LOAN_REPAYMENT')
            ->get();

            return view('pages.reports.repaid-amount',compact('repaidAmount','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }
    }
}
