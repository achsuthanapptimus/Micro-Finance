<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\DepartmentStoreRequest;
use App\Http\Requests\DepartmentUpdateRequest;
use Exception;
use RealRashid\SweetAlert\Facades\Alert;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->is('api*')) {
            $sections = Department::whereNull('parent_id')
            ->with('childrens')
                                ->orderby('name', 'asc')
                                ->get();

            return response()->json([
                "success" => true,
                "message" => "Operation successful.",
                "data" => $sections,
            ],200);
        }

        $sections = Department::whereNull('parent_id')
                                ->with('childSections')
                                ->orderby('name', 'asc')
                                ->get();

        return view('pages.department.index',compact('sections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $current = Department::find($request->pid);
        $root = $this->parentPath($current);
        $users = User::all();
        return view('pages.department.create',compact('root','current','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentStoreRequest $request)
    {
        DB::beginTransaction();
        try{
            $department = Department::create($request->validated());
            DB::commit();
            toast('Department Created Successfully','success')->timerProgressBar();
            return redirect()->route('departments.index');
        }catch (Exception $th) {
            DB::rollback();
            toast('Oops something went wrong please try again','error')->timerProgressBar();
            return redirect()->route('departments.create');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        $root = $this->parentPath($department->parent);
        $users = User::all();
        return view('pages.department.edit',compact('department','root','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(DepartmentUpdateRequest $request, Department $department)
    {
        try{
            $department->update($request->validated());
            toast('Department Created Successfully','success')->timerProgressBar();
            return redirect()->route('departments.index');
        }catch (Exception $th) {
            toast('Oops something went wrong please try again','error')->timerProgressBar();
            return redirect()->route('departments.edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        //
    }

    private function parentPath($currentNode)
    {
        $root = '';
        $patent = $currentNode;
        $roots = [];


        while($patent) {
            $roots[] = $patent->name.' ('.$patent->type.')';
            $patent = $patent->parent;
        }

        foreach (array_reverse($roots) as $key => $value) {
        $root = $root.' / '.$value;
        }
        return $root;
    }
}
