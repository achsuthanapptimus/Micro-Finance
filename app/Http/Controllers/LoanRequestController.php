<?php

namespace App\Http\Controllers;
use App\Models\LoanRequest;

use Illuminate\Http\Request;
use DB;
use DateTime;

class LoanRequestController extends Controller
{
    public function index(Request $request)
    {

        $dt = new DateTime();
        $search="";
        $xaxisVals = array();
        $yaxisVals = array();

        if($request->get('fdate')){
            $fdate = $request->get('fdate');
        }else{
            $fdate = $dt->format('Y-m-d');
        }

        if($request->get('tdate')){
            $tdate = $request->get('tdate');
        }else{
            $tdate = $dt->format('Y-m-d');
        }

        $grouping=$request->grouping;
        
        if($request->grouping == "day-wise"){
             $loanRequest = DB::table('bank_loan_requests')
            ->select(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d - %W") AS grp,sum(amount)as amount,COUNT(id) as val'))
            ->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d - %W")'))
            ->where('created_at', '>=', $fdate)
            ->where('created_at', '<=', $tdate)
            ->whereNull('deleted_at')->get();

            foreach($loanRequest as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }
            

            return view('pages.reports.loan-request',compact('loanRequest','grouping','fdate','tdate','xaxisVals','yaxisVals'));

        }

        if($request->grouping == "month-wise"){
            $loanRequest = DB::table('bank_loan_requests')
            ->select(DB::raw('DATE_FORMAT(created_at, "%Y-%m - %M") AS grp,sum(amount)as amount,COUNT(id) as val'))
            ->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y-%m - %M")'))
            ->where('created_at', '>=', $fdate)
            ->where('created_at', '<=', $tdate)
            ->whereNull('deleted_at')->get();

            foreach($loanRequest as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.loan-request',compact('loanRequest','grouping','fdate','tdate','xaxisVals','yaxisVals'));

        }

        if($request->grouping == "year-wise"){
            $loanRequest = DB::table('bank_loan_requests')
            ->select(DB::raw('DATE_FORMAT(created_at, "%Y") AS grp,COUNT(id) as val'))
            ->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y")'))
            ->where('created_at', '>=', $fdate)
            ->where('created_at', '<=', $tdate)
            ->whereNull('deleted_at')->get();

            foreach($loanRequest as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.loan-request',compact('loanRequest','grouping','fdate','tdate','xaxisVals','yaxisVals'));

        }

        if($request->grouping == "department-wise"){
            $loanRequest = DB::table('bank_loan_requests')
            ->leftjoin('core_departments','core_departments.id' ,'=','bank_loan_requests.department_id')
            ->select(DB::raw('core_departments.name as grp,sum(amount)as amount,COUNT(bank_loan_requests.id) as val'))
            ->groupBy('grp')
            ->where('bank_loan_requests.created_at', '>=', $fdate)
            ->where('bank_loan_requests.created_at', '<=', $tdate)
            ->whereNull('bank_loan_requests.deleted_at')->get();

            foreach($loanRequest as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.loan-request',compact('loanRequest','grouping','fdate','tdate','xaxisVals','yaxisVals'));

        }


        if($request->grouping == "customer-wise"){
            $loanRequest = DB::table('bank_loan_requests')
            ->leftjoin('core_users','core_users.id' ,'=','bank_loan_requests.requested_user_id')
            ->select(DB::raw('concat(core_users.title,".",core_users.first_name," ",core_users.last_name) as grp,sum(amount)as amount,COUNT(bank_loan_requests.id) as val'))
            ->groupBy('grp')
            ->where('bank_loan_requests.created_at', '>=', $fdate)
            ->where('bank_loan_requests.created_at', '<=', $tdate)
            ->whereNull('bank_loan_requests.deleted_at')->get();

            foreach($loanRequest as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.loan-request',compact('loanRequest','grouping','fdate','tdate','xaxisVals','yaxisVals'));

        }
    }
}
