<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InvestmentCreated;
use DB;
use DateTime;

class InvestmentCreatedController extends Controller
{
    //
    public function index(Request $request)
    {

        $dt = new DateTime();
        $search="";
        $xaxisVals = array();
        $yaxisVals = array();



        if($request->get('fdate')){
            $fdate = $request->get('fdate');
        }else{
            $fdate = $dt->format('Y-m-d');
        }

        if($request->get('tdate')){
            $tdate = $request->get('tdate');
        }else{
            $tdate = $dt->format('Y-m-d');
        }

        $grouping=$request->grouping;
        
        if($request->grouping == "day-wise"){
             $InvestmentCreated = DB::table('bank_investment_accounts')
            ->select(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d - %W") AS grp,sum(amount) as amount,COUNT(id) as val'))
            ->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d - %W")'))
            ->where('created_at', '>=', $fdate)
            ->where('created_at', '<=', $tdate)
            ->get();

            foreach($InvestmentCreated as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.investment-created',compact('InvestmentCreated','grouping','fdate','tdate','xaxisVals','yaxisVals'));

        }
        elseif($request->grouping == "month-wise"){
            $InvestmentCreated = DB::table('bank_investment_accounts')
            ->select(DB::raw('DATE_FORMAT(created_at, "%Y-%m - %M") AS grp,sum(amount) as amount,COUNT(id) as val'))
            ->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y-%m - %M")'))
            ->where('created_at', '>=', $fdate)
            ->where('created_at', '<=', $tdate)
            ->get();

            foreach($InvestmentCreated as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.investment-created',compact('InvestmentCreated','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "year-wise"){
            $InvestmentCreated= DB::table('bank_investment_accounts')
            ->select(DB::raw('DATE_FORMAT(created_at, "%Y") AS grp,sum(amount) as amount,COUNT(id) as val'))
            ->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y")'))
            ->where('created_at', '>=', $fdate)
            ->where('created_at', '<=', $tdate)
            ->get();

            foreach($InvestmentCreated as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.investment-created',compact('InvestmentCreated','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "department-wise"){
            $InvestmentCreated = DB::table('bank_investment_accounts')
            ->leftjoin('core_departments','core_departments.id' ,'=','bank_investment_accounts.department_id')
            ->select(DB::raw('core_departments.name AS grp,sum(amount) as amount,COUNT(bank_investment_accounts.id) as val'))
            ->groupBy('grp')
            ->where('bank_investment_accounts.created_at', '>=', $fdate)
            ->where('bank_investment_accounts.created_at', '<=', $tdate)
            ->get();

            foreach($InvestmentCreated as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.investment-created',compact('InvestmentCreated','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }
        
        elseif($request->grouping == "product-wise"){
            $InvestmentCreated = DB::table('bank_investment_accounts')
            ->leftjoin('bank_account_products','bank_account_products.id' ,'=','bank_investment_accounts.account_product_id')
            ->select(DB::raw('bank_account_products.title AS grp,sum(amount) as amount,COUNT(bank_investment_accounts.account_product_id) as val'))
            ->groupBy('grp')
            ->where('bank_investment_accounts.created_at', '>=', $fdate)
            ->where('bank_investment_accounts.created_at', '<=', $tdate)
            ->get();

            foreach($InvestmentCreated as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.investment-created',compact('InvestmentCreated','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "customer-wise"){
            $InvestmentCreated = DB::table('bank_investment_accounts')
            ->leftjoin('bank_accounts','bank_accounts.id' ,'=','bank_investment_accounts.account_id')
            ->leftjoin('bank_account_holders','bank_account_holders.account_id' ,'=','bank_accounts.id')
            ->leftjoin('core_users','core_users.id' ,'=','bank_account_holders.user_id')
            ->select(DB::raw('concat(core_users.title,".",core_users.first_name," ",core_users.last_name) as grp,sum(amount) as amount,COUNT(bank_investment_accounts.id) as val'))
            ->groupBy('grp')
            ->where('bank_investment_accounts.created_at', '>=', $fdate)
            ->where('bank_investment_accounts.created_at', '<=', $tdate)
            ->get();

            foreach($InvestmentCreated as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.investment-created',compact('InvestmentCreated','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }
    }
}
