<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;

class DepositController extends Controller
{
    //
    public function index(Request $request)
    {

        $dt = new DateTime();
        $search="";
        $xaxisVals = array();
        $yaxisVals = array();


        if($request->get('fdate')){
            $fdate = $request->get('fdate');
        }else{
            $fdate = $dt->format('Y-m-d');
        }

        if($request->get('tdate')){
            $tdate = $request->get('tdate');
        }else{
            $tdate = $dt->format('Y-m-d');
        }

        $grouping=$request->grouping;
        
        if($request->grouping == "day-wise"){
             $bankDeposits = DB::table('bank_deposits')
             ->leftjoin('bank_account_transactions','bank_account_transactions.id' ,'=','bank_deposits.account_transaction_id')
            ->select(DB::raw('DATE_FORMAT(bank_deposits.created_at, "%Y-%m-%d - %W") AS grp,SUM(bank_account_transactions.amount) as amount,COUNT(bank_deposits.id) as val'))
            ->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d - %W")'))
            ->where('bank_deposits.created_at', '>=', $fdate)
            ->where('bank_deposits.created_at', '<=', $tdate)
            ->get();

            foreach($bankDeposits as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.deposit',compact('bankDeposits','grouping','fdate','tdate','xaxisVals','yaxisVals'));

        }
        elseif($request->grouping == "month-wise"){
            $bankDeposits= DB::table('bank_deposits')
            ->leftjoin('bank_account_transactions','bank_account_transactions.id' ,'=','bank_deposits.account_transaction_id')
           ->select(DB::raw('DATE_FORMAT(bank_deposits.created_at, "%Y-%m - %M") AS grp,SUM(bank_account_transactions.amount) as amount,COUNT(bank_deposits.id) as val'))
           ->groupBy('grp')
           ->where('bank_deposits.created_at', '>=', $fdate)
           ->where('bank_deposits.created_at', '<=', $tdate)
           ->get();

           foreach($bankDeposits as $r){
            array_push($xaxisVals, $r->grp);
            array_push($yaxisVals, $r->val);
        }

           return view('pages.reports.deposit',compact('bankDeposits','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "year-wise"){
            $bankDeposits = DB::table('bank_deposits')
            ->leftjoin('bank_account_transactions','bank_account_transactions.id' ,'=','bank_deposits.account_transaction_id')
           ->select(DB::raw('DATE_FORMAT(bank_deposits.created_at, "%Y") AS grp,SUM(bank_account_transactions.amount) as amount,COUNT(bank_deposits.id) as val'))
           ->groupBy('grp')
           ->where('bank_deposits.created_at', '>=', $fdate)
           ->where('bank_deposits.created_at', '<=', $tdate)
           ->get();

           foreach($bankDeposits as $r){
            array_push($xaxisVals, $r->grp);
            array_push($yaxisVals, $r->val);
        }

           return view('pages.reports.deposit',compact('bankDeposits','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "department-wise"){
            $bankDeposits = DB::table('bank_deposits')
            ->leftjoin('bank_account_transactions','bank_account_transactions.id' ,'=','bank_deposits.account_transaction_id')
            ->leftjoin('core_departments','core_departments.id' ,'=','bank_deposits.department_id')
            ->select(DB::raw('core_departments.name AS grp,SUM(bank_account_transactions.amount) as amount,COUNT(bank_deposits.id) as val'))
            ->groupBy('grp')
            ->where('bank_deposits.created_at', '>=', $fdate)
            ->where('bank_deposits.created_at', '<=', $tdate)
            ->get();

            foreach($bankDeposits as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.deposit',compact('bankDeposits','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "account-wise"){
            $bankDeposits = DB::table('bank_deposits')
            ->leftjoin('bank_account_transactions','bank_account_transactions.id' ,'=','bank_deposits.account_transaction_id')
            ->leftjoin('bank_accounts','bank_accounts.id' ,'=','bank_account_transactions.account_id')
            ->select(DB::raw('bank_accounts.account_number AS grp,SUM(bank_account_transactions.amount) as amount,COUNT(bank_deposits.id) as val'))
            ->groupBy('grp')
            ->where('bank_deposits.created_at', '>=', $fdate)
            ->where('bank_deposits.created_at', '<=', $tdate)
            ->get();

            foreach($bankDeposits as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.deposit',compact('bankDeposits','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "customer-wise"){
            $bankDeposits = DB::table('bank_deposits')
            ->leftjoin('bank_account_transactions','bank_account_transactions.id' ,'=','bank_deposits.account_transaction_id')
            ->leftjoin('bank_account_holders','bank_account_holders.account_id' ,'=','bank_account_transactions.account_id')
            ->leftjoin('core_users','core_users.id' ,'=','bank_account_holders.user_id')
            ->select(DB::raw('concat(core_users.title,".",core_users.first_name," ",core_users.last_name) as grp,SUM(bank_account_transactions.amount) as amount,COUNT(bank_deposits.id) as val'))
            ->groupBy('grp')
            ->where('bank_deposits.created_at', '>=', $fdate)
            ->where('bank_deposits.created_at', '<=', $tdate)
            ->get();

            foreach($bankDeposits as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.deposit',compact('bankDeposits','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }
    }
}
