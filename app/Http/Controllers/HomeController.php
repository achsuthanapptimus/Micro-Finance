<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Department;
use App\Models\Role;
use App\Models\Module;
use Redirect,Response;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $user = $request->user();
        if ($request->session()->has('department')) {
            $departments_id = $request->session()->get('department');
            $roles = $request->user()->userRole->where('departments_id',$departments_id)->first();
            if ( $roles) {
                $roles = $roles->role;
                $session = $user->userRole;
            }else{
                $roles = $user->mainRole->first();
                $section = Department::find($roles->pivot->departments_id);
                $request->session()->put('department', $roles->pivot->departments_id);
            }
        }else{
            $roles = $user->mainRole->first();
            $section = Department::find($roles->pivot->departments_id);
            $request->session()->put('department', $roles->pivot->departments_id);
        }

        $all_module = Module::all();
        $module = array();
        if ($roles && $roles->is_admin == true) {
            $module = $all_module->pluck('id')->toArray();           
        }else{
            $permissions =  $roles->permissions;
            foreach ($permissions as $permission){
                if(!in_array($permission->modules_id, $module, true)){
                    array_push($module, $permission->modules_id);
                }
            }
        }

        $module = Module::all()->whereIn('id',$module);
        setPermissions();
        return view('home',compact('module'));
    }

    public function moduleRedirect(Request $request,$moduleId) 
    {
        try {
                $user = $request->user();
                $token = $user->token;

                $did = $request->session()->get('department');
                $module = Module::find($moduleId);
            
                $url = $module->login_url."?token=".$token."&module=".$module->id."&did=".$did;
                return Redirect::to($url);

        } catch (\Exception $th) {
            DB::rollback();
            return Auth::logout();
        }
    }
}
