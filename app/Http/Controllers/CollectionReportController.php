<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\DepartmentStoreRequest;
use App\Http\Requests\DepartmentUpdateRequest;
use Exception;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use DateTime;


class CollectionReportController extends Controller
{
     public function collectionReport(Request $request)
    {

        $dt = new DateTime();
        $search="";
        $officer=$request->officer;
        $center=$request->center;
        $department=$request->department;
        $product=$request->product;

        if($request->get('fdate')){
            $fdate = $request->get('fdate');
        }else{
            $fdate = $dt->format('Y-m-d');
        }

        if($request->get('tdate')){
            $tdate = $request->get('tdate');
        }else{
            $tdate = $dt->format('Y-m-d');
        }



        $collectionReport = DB::table('bank_repayments')
            ->select(DB::raw('core_users.first_name AS name,bank_csus.name as collectionCenter,COUNT(bank_repayments.id) as totalCollections,SUM(collected_capital+collected_interest) as totalAmount'))
            ->leftjoin('core_users','core_users.id' ,'=','bank_repayments.collected_by')
            ->leftjoin('bank_loan_accounts','bank_loan_accounts.id' ,'=','bank_repayments.loan_account_id')
            ->leftjoin('bank_csus','bank_csus.id' ,'=','bank_loan_accounts.csu_id')
            ->leftjoin('core_departments','core_departments.id' ,'=','bank_repayments.department_id')
            ->where('bank_repayments.created_at', '>=', $fdate)
            ->where('bank_repayments.created_at', '<=', $tdate)
            ->groupBy('core_users.first_name','bank_csus.name');
        
        if(isset($officer) && $officer != "") {
            $collectionReport = $collectionReport ->where('bank_repayments.collected_by','=',$officer);
        }

        if(isset($center) && $center != "") {
            $collectionReport = $collectionReport ->where('bank_csus.id','=',$center);
        }

        if(isset($department) && $department != "") {
            $collectionReport = $collectionReport ->where('bank_csus.department_id','=',$department);
        }

        $collectionReport = $collectionReport->get();

        $officers = DB::table('core_users')
            ->select(DB::raw('id,first_name as name'))
            ->get();

        $csus = DB::table('bank_csus')
            ->select(DB::raw('id,name'))
            ->get();

        $departments = DB::table('core_departments')
            ->select(DB::raw('id,name'))
            ->get();

        return view('pages.reports.collection-report',compact('collectionReport','fdate','tdate','csus','officers','departments','officer','center','department'));
    }
  
    public function officerWise(Request $request)
    {
        $dt = new DateTime();
        $search="";
        $center=$request->center;

        if($request->get('fdate')){
            $fdate = $request->get('fdate');
        }else{
            $fdate = $dt->format('Y-m-d');
        }

        if($request->get('tdate')){
            $tdate = $request->get('tdate');
        }else{
            $tdate = $dt->format('Y-m-d');
        }



        $collectionByOfficer = DB::table('bank_repayments')
            ->select(DB::raw('core_users.first_name AS name,bank_csus.name as collectionCenter,COUNT(bank_repayments.id) as totalCollections,SUM(collected_capital+collected_interest) as totalAmount'))
            ->leftjoin('core_users','core_users.id' ,'=','bank_repayments.collected_by')
            ->leftjoin('bank_loan_accounts','bank_loan_accounts.id' ,'=','bank_repayments.loan_account_id')
            ->leftjoin('bank_csus','bank_csus.id' ,'=','bank_loan_accounts.csu_id')
            ->where('bank_repayments.created_at', '>=', $fdate)
            ->where('bank_repayments.created_at', '<=', $tdate)
            ->groupBy('core_users.first_name','bank_csus.name');
        
        if(isset($center) && $center != "") {
            $collectionByOfficer = $collectionByOfficer ->where('bank_csus.id','=',$center);
        }

        $collectionByOfficer = $collectionByOfficer->get();


        $csus = DB::table('bank_csus')
            ->select(DB::raw('id,name'))
            ->get();

        return view('pages.reports.officer-wise-collection',compact('collectionByOfficer','fdate','tdate','csus','center'));
    }

    public function centerWise(Request $request)
    {
        $dt = new DateTime();
        $search="";

        if($request->get('fdate')){
            $fdate = $request->get('fdate');
        }else{
            $fdate = $dt->format('Y-m-d');
        }

        if($request->get('tdate')){
            $tdate = $request->get('tdate');
        }else{
            $tdate = $dt->format('Y-m-d');
        }



        $collectionByCenter = DB::table('bank_repayments')
            ->select(DB::raw('bank_csus.name as collectionCenter,COUNT(bank_repayments.id) as totalCollections,SUM(collected_capital+collected_interest) as totalAmount'))
            ->leftjoin('bank_loan_accounts','bank_loan_accounts.id' ,'=','bank_repayments.loan_account_id')
            ->leftjoin('bank_csus','bank_csus.id' ,'=','bank_loan_accounts.csu_id')
            ->where('bank_repayments.created_at', '>=', $fdate)
            ->where('bank_repayments.created_at', '<=', $tdate)
            ->groupBy('bank_csus.name')
            ->get();

        return view('pages.reports.center-wise-collection',compact('collectionByCenter','fdate','tdate'));
    }

    public function departmentWise(Request $request)
    {
        $dt = new DateTime();
        $search="";

        if($request->get('fdate')){
            $fdate = $request->get('fdate');
        }else{
            $fdate = $dt->format('Y-m-d');
        }

        
        if($request->get('tdate')){
            $tdate = $request->get('tdate');
        }else{
            $tdate = $dt->format('Y-m-d');
        }



        $collectionByDepartment = DB::table('bank_repayments')
            ->select(DB::raw('core_departments.name as department,COUNT(bank_repayments.id) as totalCollections,SUM(collected_capital+collected_interest) as totalAmount'))
            ->leftjoin('core_departments','core_departments.id' ,'=','bank_repayments.department_id')
            ->where('bank_repayments.created_at', '>=', $fdate)
            ->where('bank_repayments.created_at', '<=', $tdate)
            ->groupBy('core_departments.name')
            ->get();

        return view('pages.reports.department-wise-collection',compact('collectionByDepartment','fdate','tdate'));
    }
   
}
