<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;

class InterestGivenController extends Controller
{
    public function index(Request $request)
    {

        $dt = new DateTime();
        $search="";
        $xaxisVals = array();
        $yaxisVals = array();



        if($request->get('fdate')){
            $fdate = $request->get('fdate');
        }else{
            $fdate = $dt->format('Y-m-d');
        }

        if($request->get('tdate')){
            $tdate = $request->get('tdate');
        }else{
            $tdate = $dt->format('Y-m-d');
        }

        $grouping=$request->grouping;
        
        if($request->grouping == "day-wise"){
             $interestGiven = DB::table('bank_saving_accounts')
            ->leftjoin('bank_accounts','bank_accounts.id' ,'=','bank_saving_accounts.account_id')
            ->leftjoin('bank_account_products','bank_account_products.id' ,'=','bank_saving_accounts.account_product_id')
            ->select(DB::raw('DATE_FORMAT(bank_saving_accounts.created_at, "%Y-%m-%d - %W") AS grp,
            IFNULL(bank_accounts.overwritten_interest_rate,bank_account_products.default_interest_rate) as val'))
            ->groupBy('grp','val')
            ->where('bank_saving_accounts.created_at', '>=', $fdate)
            ->where('bank_saving_accounts.created_at', '<=', $tdate)
            ->get();

            foreach($interestGiven  as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.interest-given',compact('interestGiven','grouping','fdate','tdate','xaxisVals','yaxisVals'));

        }
        elseif($request->grouping == "month-wise"){
            $interestGiven = DB::table('bank_saving_accounts')
            ->leftjoin('bank_accounts','bank_accounts.id' ,'=','bank_saving_accounts.account_id')
            ->leftjoin('bank_account_products','bank_account_products.id' ,'=','bank_saving_accounts.account_product_id')
            ->select(DB::raw('DATE_FORMAT(bank_saving_accounts.created_at, "%Y-%m - %M") AS grp,
            IFNULL(bank_accounts.overwritten_interest_rate,bank_account_products.default_interest_rate) as val'))
            ->groupBy('grp','val')
            ->where('bank_saving_accounts.created_at', '>=', $fdate)
            ->where('bank_saving_accounts.created_at', '<=', $tdate)
            ->get();

            foreach($interestGiven  as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.interest-given',compact('interestGiven','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "year-wise"){
            $interestGiven = DB::table('bank_saving_accounts')
            ->leftjoin('bank_accounts','bank_accounts.id' ,'=','bank_saving_accounts.account_id')
            ->leftjoin('bank_account_products','bank_account_products.id' ,'=','bank_saving_accounts.account_product_id')
            ->select(DB::raw('DATE_FORMAT(bank_saving_accounts.created_at, "%Y") AS grp,
            IFNULL(bank_accounts.overwritten_interest_rate,bank_account_products.default_interest_rate) as val'))
            ->groupBy('grp','val')
            ->where('bank_saving_accounts.created_at', '>=', $fdate)
            ->where('bank_saving_accounts.created_at', '<=', $tdate)
            ->get();

            foreach($interestGiven  as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.interest-given',compact('interestGiven','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "department-wise"){
            $interestGiven = DB::table('bank_saving_accounts')
            ->leftjoin('bank_accounts','bank_accounts.id' ,'=','bank_saving_accounts.account_id')
            ->leftjoin('bank_account_products','bank_account_products.id' ,'=','bank_saving_accounts.account_product_id')
            ->leftjoin('core_departments','core_departments.id' ,'=','bank_saving_accounts.department_id')
            ->select(DB::raw('core_departments.name AS grp,
            IFNULL(bank_accounts.overwritten_interest_rate,bank_account_products.default_interest_rate) as val'))
            ->groupBy('grp','val')
            ->where('bank_saving_accounts.created_at', '>=', $fdate)
            ->where('bank_saving_accounts.created_at', '<=', $tdate)
            ->get();

            foreach($interestGiven  as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }
            return view('pages.reports.interest-given',compact('interestGiven','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "account-wise"){

            $interestGiven = DB::table('bank_saving_accounts')
            ->leftjoin('bank_accounts','bank_accounts.id' ,'=','bank_saving_accounts.account_id')
            ->leftjoin('bank_account_products','bank_account_products.id' ,'=','bank_saving_accounts.account_product_id')
            ->select(DB::raw('bank_accounts.account_number AS grp,
            IFNULL(bank_accounts.overwritten_interest_rate,bank_account_products.default_interest_rate) as val'))
            ->groupBy('grp','val')
            ->where('bank_saving_accounts.created_at', '>=', $fdate)
            ->where('bank_saving_accounts.created_at', '<=', $tdate)
            ->get();

            foreach($interestGiven  as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.interest-given',compact('interestGiven','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }

        elseif($request->grouping == "customer-wise"){
            $interestGiven = DB::table('bank_saving_accounts')
            ->leftjoin('bank_accounts','bank_accounts.id' ,'=','bank_saving_accounts.account_id')
            ->leftjoin('bank_account_products','bank_account_products.id' ,'=','bank_saving_accounts.account_product_id')
            ->leftjoin('bank_account_holders','bank_account_holders.account_id' ,'=','bank_saving_accounts.id')
            ->leftjoin('core_users','core_users.id' ,'=','bank_account_holders.user_id')
            ->select(DB::raw('concat(core_users.title,".",core_users.first_name," ",core_users.last_name) AS grp,
            IFNULL(bank_accounts.overwritten_interest_rate,bank_account_products.default_interest_rate) as val'))
            ->groupBy('grp','val')
            ->where('bank_saving_accounts.created_at', '>=', $fdate)
            ->where('bank_saving_accounts.created_at', '<=', $tdate)
            ->get();

            foreach($interestGiven  as $r){
                array_push($xaxisVals, $r->grp);
                array_push($yaxisVals, $r->val);
            }

            return view('pages.reports.interest-given',compact('interestGiven','grouping','fdate','tdate','xaxisVals','yaxisVals'));
        }
    }
}
