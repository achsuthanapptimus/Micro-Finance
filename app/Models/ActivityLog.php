<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class ActivityLog extends Model
{
    use HasFactory;
    protected $table = 'core_activity_log';
    
    protected $keyType = 'string';
    public $incrementing = false;

    public static function boot()
    {
        parent::boot();
        static::creating(function($user)
        {
            $user->id = uuid4();

            if(Auth::check()){
                $user->created_by = Auth::user()->id;
                $user->updated_by = Auth::user()->id;
            }
        });

        static::updating(function($user)
        {
            if(Auth::check()){
                $user->updated_by = Auth::user()->id;
            }
        });

        static::deleting(function($user)
        {
            if(Auth::check()){
                $user->deleted_by = Auth::user()->id;
            }
        });
    }
}
