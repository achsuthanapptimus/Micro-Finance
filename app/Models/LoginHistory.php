<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class LoginHistory extends Model
{
    use HasFactory;
    protected $table = 'core_login_histories';
    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = [
        'device',
        'browser',
        'status',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($department) {
            $department->id = uuid4();

            if (Auth::check()) {
                $department->added_by = Auth::user()->id;
                $department->updated_by = Auth::user()->id;
            }
        });

        static::updating(function ($department) {
            if (Auth::check()) {
                $department->updated_by = Auth::user()->id;
            }
        });

        static::deleting(function ($department) {
            if (Auth::check()) {
                $department->deleted_by = Auth::user()->id;
                $department->save();
            }
        });
    }
}
