<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Role extends Model
{
    use HasFactory;
    protected $keyType = 'string';
    public $incrementing = false;
    protected $table = 'core_roles';

    protected $fillable = [
         'name',
         'created_at',
         'deleted_at',
         'updated_at',
         'added_by',
         'deleted_by',
         'updated_by'
      ];

      public static function boot()
      {
          parent::boot();
          static::creating(function($user)
          {
              $user->id = uuid4();
  
              if(Auth::check()){
                  $user->added_by = Auth::user()->id;
                  $user->updated_by = Auth::user()->id;
              }
          });
  
          static::updating(function($user)
          {
              if(Auth::check()){
                  $user->updated_by = Auth::user()->id;
              }
          });
  
          static::deleting(function($user)
          {
              if(Auth::check()){
                  $user->deleted_by = Auth::user()->id;
              }
          });
      }

    public function permissions() {
        return $this->belongsToMany(Permission::class,'core_roles_permissions','roles_id','permissions_id');
     }

     public function users() {
        return $this->belongsToMany(User::class,'core_users_roles');
     }
}
