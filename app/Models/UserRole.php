<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    use HasFactory;
    protected $table = 'core_users_roles';
    public $timestamps = false;

    protected $fillable = ['users_id','roles_id','departments_id','is_main'];

    public static function boot()
    {
        parent::boot();
        static::creating(function($data)
        {
            $data->id = uuid4();
        });
    }

    public function department()
    {
        return $this->hasOne(Department::class,'id','departments_id');
    }

    public function role()
    {
        return $this->hasOne(Role::class,'id','roles_id');
    }

    public function users()
    {
        return $this->belongsTo(User::class,'users_id','id');
    }


}
