<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\Client;

class AuthClient extends Client
{
    protected $table = 'core_oauth_clients';
}
