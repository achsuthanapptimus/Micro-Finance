<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Permissions\HasPermissionsTrait;

class Party extends Authenticatable
{
    use HasApiTokens,HasPermissionsTrait, HasFactory, Notifiable;
    protected $table = 'core_parties';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = ['user_id','type'];

    
    protected $keyType = 'string';
    public $incrementing = false;

    public static function boot()
    {
        parent::boot();
        static::creating(function($user)
        {
            $user->id = uuid4();

            if(Auth::check()){
                $user->created_by = Auth::user()->id;
                $user->updated_by = Auth::user()->id;
            }
        });

        static::updating(function($user)
        {
            if(Auth::check()){
                $user->updated_by = Auth::user()->id;
            }
        });

        static::deleting(function($user)
        {
            if(Auth::check()){
                $user->deleted_by = Auth::user()->id;
            }
        });
    }

 
}
