<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Department extends Model
{
    use HasFactory;
    protected $table = 'core_departments';
    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = [
            'name',
            'code_name',
            'type',
            'manager_id',
            'supervisor_id',
            'parent_id',
            'logo',
            'created_by',
            'updated_by',
            'deleted_by'
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($department) {
            $department->id = uuid4();
            $department->code_name = 'SE-'.(string)(100001+Department::count());

            if (Auth::check()) {
                $department->created_by = Auth::user()->id;
                $department->updated_by = Auth::user()->id;
            }
        });

        static::updating(function ($department) {
            if (Auth::check()) {
                $department->updated_by = Auth::user()->id;
            }
        });

        static::deleting(function ($department) {
            if (Auth::check()) {
                $department->deleted_by = Auth::user()->id;
                $department->save();
            }
        });
    }

    public function setLogoAttibute($value) {
        if ($value) {
            $imageName = time().'.'.$value->extension();  
            $value->storeAs('logo', $imageName);
            $this->attributes['logo'] = '/logo/'.$imageName;
        }

        $this->attributes['logo'] = null;
    }

    public function parent() {
        return $this->belongsTo(Department::class,'parent_id');
    }

     // this relationship will only return one level of child items
     public function sections(){
         return $this->hasMany(Department::class, 'parent_id');
     }
 
     // This is method where we implement recursive relationship
     public function childSections(){
         return $this->hasMany(Department::class, 'parent_id')->with('sections');
     }
     
     public function childrens(){
        return $this->sections()->with('childrens');
    }
}
