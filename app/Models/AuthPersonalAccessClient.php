<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\PersonalAccessClient;
use Illuminate\Support\Str;

class AuthPersonalAccessClient extends PersonalAccessClient
{
    protected $table = 'core_oauth_personal_access_clients';

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (config('passport.client_uuids')) {
                $model->{$model->getKeyName()} = $model->{$model->getKeyName()} ?: (string) Str::orderedUuid();
            }
        });
    }
}
