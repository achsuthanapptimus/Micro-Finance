<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\RefreshToken;

class AuthRefreshToken extends RefreshToken
{
    protected $table = 'core_oauth_refresh_tokens';
}
