<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\AuthCode;

class AuthAuthCode extends AuthCode
{
    protected $keyType = 'string';
    public $timestamps = false;
    protected $table = 'core_oauth_auth_codes';
}
