<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Permissions\HasPermissionsTrait;

class LoanRequest extends Authenticatable
{
    use HasApiTokens,HasPermissionsTrait, HasFactory, Notifiable;
    protected $table = 'bank_loan_requests';
    
    protected $keyType = 'string';
    public $incrementing = false;

 
}
