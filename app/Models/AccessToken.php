<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\Token;

class AccessToken extends Token
{
    protected $table = 'core_oauth_access_tokens';
}
