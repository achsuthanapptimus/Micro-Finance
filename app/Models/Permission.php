<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use HasFactory;
    protected $keyType = 'string';
    public $timestamps = false;
    protected $table = 'core_permissions';

    public function roles() {
        return $this->belongsToMany(Role::class,'core_roles_permissions','permissions_id','roles_id');
    }
}
