<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Auth;
use App\Permissions\HasPermissionsTrait;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens,HasPermissionsTrait, HasFactory, Notifiable;
    protected $table = 'core_users';
    protected $appends = ['full_name'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = ['code','contact_primary','contact_secondary','maritial_status','nationality','created_at',
        'deleted_at','dob','dp','email','first_name','gender','identifier','identifier_value',
        'is_profile_complete','landline','language','last_name','middle_name','nic_no','passport_no',
        'password','qualification','remember_token','status','title','token','updated_at','username',
        'created_by','deleted_by','department_id','supervisor_id','updated_by','representative_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $keyType = 'string';
    public $incrementing = false;

    public static function boot()
    {
        parent::boot();
        static::creating(function($user)
        {
            $user->id = uuid4();

            if(Auth::check()){
                $user->created_by = Auth::user()->id;
                $user->updated_by = Auth::user()->id;
            }
        });

        static::updating(function($user)
        {
            if(Auth::check()){
                $user->updated_by = Auth::user()->id;
            }
        });

        static::deleting(function($user)
        {
            if(Auth::check()){
                $user->deleted_by = Auth::user()->id;
            }
        });
    }

    public function getFullNameAttribute() {
        return ($this->title != null ? $this->title . ' ': '') . $this->first_name . ' ' . $this->middle_name. ' ' .$this->last_name ;
    }

    public function userRole()
    {
        return $this->hasMany(UserRole::Class,'users_id','id');
    }

    public function mainRole()
    {
        return $this->belongsToMany(Role::class,'core_users_roles','users_id','roles_id')
                    ->wherePivot('is_main', true)
                    ->withPivot([ 'departments_id', 'is_main' ]);
    }

    public function sectionRole()
    {
        return $this->belongsToMany(Role::class,'core_users_roles','users_id','roles_id')
                    ->withPivot([ 'departments_id', 'is_main' ]);
    }

    public function departments(){
        return $this->hasMany(UserRole::class,'users_id','id');
    }

    public function addedBy() {
        return $this->belongsTo(User::class,'added_by');
    }

    public function updateBy() {
        return $this->belongsTo(User::class,'updated_by');
    }
    public function deleteBy() {
        return $this->belongsTo(User::class,'delete_by');
    }
}
