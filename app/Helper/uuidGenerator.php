<?php

// https://tools.ietf.org/html/rfc4122#section-4.4
function uuid4() {
    $data = openssl_random_pseudo_bytes(16, $secure);
    if (false === $data) { return false; }
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}

// https://tools.ietf.org/html/rfc4122#section-4.3
function uuid5($name) {
    $hash = sha1($name, false);
    return sprintf(
        '%s-%s-5%s-%s-%s',
        substr($hash,  0,  8),
        substr($hash,  8,  4),
        substr($hash, 17,  3),
        substr($hash, 24,  4),
        substr($hash, 32, 12)
    );
}