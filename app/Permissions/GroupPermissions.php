<?php
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;

function getPermissions($group)
{
    $permissions = [];
    if (Session::has('permissions')) {
        $groups = explode(',', $group);
        $allPermissions =  Session::get('permissions');
        foreach ($allPermissions as $key => $value) {
            if( in_array( $value ,$groups )){
                $permissions[] = $key;
            }
        }
    }
    return $permissions;
}

function getPermissionsByName($name)
{
    if (Session::has('permissions')) {
        $allPermissions =  Session::get('permissions');
        if (array_key_exists($name, $allPermissions)) {
            return true;
        }
    }
    return false;
}


function isPermissions($names)
{
    $groups = explode(',', $names);
    foreach ($groups as $key => $value) {
        if (getPermissionsByName($value)) {
            return true;
        }
    }
    return false;
}

function isAndPermissions($names)
{
    $groups = explode(',', $names);
    $count = true;
    foreach ($groups as $key => $value) {
        if (!getPermissionsByName($value)) {
            $count = false;
        }
    }
    return $count;
}


function setPermissions()
{
    $role = Auth::user()->sectionRole()->where('departments_id', Session::get('department'))->first();
    if ($role) {
            if ($role->is_admin == true) {
                $permissions = Permission::all();
                $permissions = $permissions->pluck('group_by','name')->toArray();
            }else{
                $permissions = $role->permissions->pluck('group_by','name')->toArray();
            }
            Session::put('permissions',$permissions);
    }
    // return [];
}