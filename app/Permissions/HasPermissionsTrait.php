<?php

namespace App\Permissions;

use App\Models\Permission;
use App\Models\Role;
use Session;

trait HasPermissionsTrait {

  public function hasPermissionTo($permission) {
    return $this->hasPermissionThroughRole($permission);
  }

  public function hasBladePermissionTo($permission) {

    $permission = Permission::where('name',$permission)->first();
    if (!$permission){
      return false;
    }
    return $this->hasPermissionThroughRole($permission);
  }

  public function hasPermissionThroughRole($permission) {

    foreach ($permission->roles as $role){
      if ($role->is_admin == 1) {
        return true;
      }
      if($this->roles->contains($role)) {
        return true;
      }
    }
    return false;
  }

  public function hasRole( ... $roles ) {
    foreach ($roles as $role) {
      if ($this->roles->contains('name', $role)) {
        return true;
      }
    }
    return false;
  }

  public function roles() {
    // if (!Session::has('department')) {
    //   # code...
    // }
    return $this->belongsToMany(Role::class,'core_users_roles','users_id','roles_id')->wherePivot('departments_id', Session::get('department'));
  }

  protected function getAllPermissions(array $permissions) {
    return Permission::whereIn('name',$permissions)->get();
  }

}