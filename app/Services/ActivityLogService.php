<?php

namespace App\Services;

use App\Models\ActivityLog;
use Auth;
use DB;

class ActivityLogService {

    public function save($action_type, $type, $module, $item_uid, 
                        $log_title, $description, $url, $extra_info, $ip_address){
        $activityLog = new ActivityLog();
        $activityLog->user_id = Auth::user() ? Auth::user()->id : null;
        $activityLog->action_type = $action_type;
        $activityLog->type = $type;
        $activityLog->module = $module;
        $activityLog->item_uid = $item_uid;
        $activityLog->log_title = $log_title;
        $activityLog->description = $description;
        $activityLog->extra_info = $extra_info;
        $activityLog->url = $url;
        $activityLog->ip_address = $ip_address;
        $activityLog->save();
        return true;
    }
}
