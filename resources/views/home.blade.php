@extends('layouts.app',['webView'=>true])

@section('content')

<div class="app-container mt-5">
    <div class="d-flex justify-content-center align-items-center" >
        @foreach ($module as $item)
        <a href="{{ route('module-login', ['module' => $item->id]) }}">
            <div class="d-flex justify-content-center align-items-center bg-white shadow-md rounded text-center m-2 p-2" style="width: 200px;height: 200px;">
                <h3>{{$item->name}}</h3>
            </div>
        </a>
        @endforeach
    </div>
</div>
@endsection
