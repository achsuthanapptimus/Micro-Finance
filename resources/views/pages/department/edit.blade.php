@extends("layouts.app")

@section('title')
    Create Brand
@endsection

@section('content')
    <h1 class="page-heading rounded d-flex justify-content-between">
        Edit Departments
        <span class="text-nowrap bd-highlight fs-6 fw-normal">{{$root}}</span>
    </h1>

    <form novalidate action="{{ route('departments.update',$department->id) }}" method="POST" enctype='multipart/form-data'>
        @csrf
        @method('put')
        <div class="row" id="filter-panel">
            <div class="col-12">
                <div class="my-3 p-3 bg-body rounded shadow-sm">
                    <div class="row">
                        {{-- <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label for="code_name" class="form-label">Code Name <strong
                                        style="color:red">*</strong></label>
                                <input class="form-control mx-1 @error('code_name') is-invalid @enderror"
                                    value="{{ old('code_name') }}" placeholder="Code Name" type="text" name="code_name" />
                                @error('code_name')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> --}}
                        <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label for="name" class="form-label">Name <strong style="color:red">*</strong></label>
                                <input class="form-control mx-1 @error('name') is-invalid @enderror"
                                    value="{{ old('name', $department->name) }}" placeholder="Name" type="text" name="name" />
                                @error('name')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label for="logo" class="form-label">Logo <strong style="color:red">*</strong></label>
                                <input class="form-control mx-1 @error('logo') is-invalid @enderror"
                                     placeholder="Logo" type="file" name="logo" />
                                @error('logo')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label for="type" class="form-label">Type <strong style="color:red">*</strong></label>
                                <input class="form-control mx-1 @error('type') is-invalid @enderror"
                                    value="{{ old('type',$department->type) }}" placeholder="Type" type="text" name="type" />
                                @error('type')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label for="manager_id" class="form-label">Manager <strong
                                        style="color:red">*</strong></label>
                                <select class="searchable-select form-control @error('manager_id') is-invalid @enderror"
                                    placeholder="Manager" type="text" name="manager_id" id="manager_id">
                                    <option value="">Any</option>
                                    @foreach ($users as $item)
                                    <option value="{{$item->id}}" {{ old("manager_id",$department->manager_id) == $item->id ? "selected":"" }}>{{$item->first_name}}</option>
                                    @endforeach
                                </select>
                                @error('manager_id')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label for="supervisor_id" class="form-label">Supervisor <strong
                                        style="color:red">*</strong></label>
                                <select class="searchable-select form-control @error('supervisor_id') is-invalid @enderror"
                                    placeholder="Supervisor" type="text" name="supervisor_id" id="supervisor_id">
                                    <option value="">Any</option>
                                    @foreach ($users as $item)
                                    <option value="{{$item->id}}" {{ old("supervisor_id",$department->supervisor_id) == $item->id ? "selected":"" }}>{{$item->first_name}}</option>
                                    @endforeach
                                </select>
                                @error('supervisor_id')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        {{-- <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label for="description" class="form-label">Description</label>
                                <textarea class="form-control mx-1" placeholder="Description" type="text"
                                    name="description">{{ old('description') }}</textarea>
                            </div>
                        </div> --}}

                    </div>
                    <div class="row border-top-1 m-0 pt-4 mt-2">
                        <div class="col-12 col-md-3 offset-md-9 d-flex">
                            <button type="button" onclick="window.location.replace(`{{ route('departments.index') }}`)"
                                class="btn btn-primary-inverse w-100 mx-1">BACK</button>
                            <button type="submit" class="btn btn-primary w-100 mx-1">SUBMIT</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script>
    </script>
@endsection
