@extends("layouts.app")

@section('title')
    Brands
@endsection

@section('content')
    {{-- <form novalidate action="" method="GET" onsubmit="$('.page-loader').show()">
        <div class="row">
            <div class="col-md-4">
                <h1 class="page-heading rounded">Brands <span
                        class="badge rounded-pill bg-warning text-white ms-3">{{ $data->total() }}</span></h1>
            </div>
            <div class="col-md-8">
                <div class="filter-cont">
                    <div class="row">
                        <div class="col-md-6 col-6 d-flex search-box">
                            <input type="text" class="form-control" name="search" value="{{ $search }}"
                                placeholder="Search" />
                            <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                        </div>
                        <div class="col-md-2 col-4">
                            <button type="button" class="btn btn-primary btn-primary-inverse mt-2 mt-md-0" id="btn-filter">
                                <i class="fas fa-filter"></i> FILTER
                            </button>
                        </div>
                        <div class="col-md-3 col-3">
                            <a href="{{ route('brands.create') }}" class="btn btn-primary btn-primary-inverse float-end">
                                <i class="fas fa-plus mx-1"></i> <span class="">ADD NEW</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="display:none" id="filter-panel">
            <div class="col-12">
                <div class="my-3 p-3 bg-body rounded shadow-sm">
                    @include('include.filter', [
                        'sortBy' => $sortBy,
                        'sortDirection' => $sortDirection,
                        'pagePer' => $pagePer,
                        'sortByArray' => ['name' => 'Name', 'description' => 'Description'],
                    ])
                    <div class="row border-top-1 pt-4 mt-2">
                        <div class="col-12 col-md-3 offset-md-9 d-flex">
                            <button type="reset" class="btn btn-primary-inverse w-100 mx-1">RESET</button>
                            <button type="submit" class="btn btn-primary w-100 mx-1">SUBMIT</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form> --}}

    <div class="row mt-1">
        <div class="col-12">
            <div class="my-3 p-1 px-3 bg-white rounded shadow-md ">
                <div class="tree">
                    @if (count($sections) > 0)
                        <ul class="tree-section">
                            {{-- {{dd($sections)}} --}}
                            @foreach ($sections as $section)
                                <li id="{{ $section->name }}">
                                    <span class="d-flex align-items-center justify-content-between">
                                        {{-- <div><i class="fas fa-circle" style="color:#{{App\Enums\SectionType::getValue(str_replace(" ","",ucwords($section->type)))}}"></i> {{ $section->name }}</div> --}}
                                        <div class="d-flex">
                                            <div ><i class="fas fa-circle" style="color:black"></i></div>
                                            <div class="ps-2">{{ $section->name }}</div>
                                            <div style="color:gray;padding-left:1px">{{ ' | ' . $section->type }}</div>
                                        </div>
                                        <div class="form-inline" style="padding-right:5px">
                                            <a style="margin-right: 5px"
                                                href="{{ route('departments.create', ['pid' => $section->id]) }}"
                                                class="btn btn-primary btn-sm"><i class="fas fa-plus"></i></a>
                                            <a style="margin-right: 5px"
                                                href="{{ route('departments.edit', ['department' => $section->id, 'type' => 'main']) }}"
                                                class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                                            <a style="margin-right: 5px"
                                                href="{{ route('departments.show', $section->id) }}"
                                                class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                            <button myid='{{ $section->id }}'
                                                onclick="archiveFunction('{{ $section->type . '-' . $section->name }}','{{ $section->id }}')"
                                                style="margin-right: 5px"
                                                href=""
                                                class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                            <a hidden id="{{ $section->id }}" style="margin-right: 5px"
                                                href=""
                                                class="btn btn-danger btn-sm deldep"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </span>
                                    @if (count($section->childSections))
                                        <ul>
                                            @foreach ($section->childSections as $childSections)
                                                @include(
                                                    'pages.department.sub_sections',
                                                    ['sub_sections' => $childSections]
                                                )
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('assets/js/tree.js') }}"></script>
@endpush

@push('head')
    <link href="{{ asset('assets/styles/tree.css') }}" rel="stylesheet" />
@endpush
