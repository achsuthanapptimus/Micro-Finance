<li id="{{$sub_sections->name}}">
  <span class="d-flex align-items-center justify-content-between">
    <div id={{$sub_sections->name}} class="d-flex">
      <div ><i class="fas fa-circle" style="color:black"></i></div>
      <div class="ps-2">{{ $sub_sections->name }}</div>
      <div style="color:gray;padding-left:1px">{{ ' | ' . $sub_sections->type }}</div>
  </div>
    <div>
      <a href="{{route('departments.create',['pid' =>$sub_sections->id])}}"class="btn btn-primary btn-sm"><i class="fas fa-plus"></i></a>
      <a  href="{{route('departments.edit',['department' =>$sub_sections->id, 'type' =>'sub'])}}" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
      <a href="{{route('departments.show',$sub_sections->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
      <button myid='{{$sub_sections->id}}' onclick="archiveFunction('{{$sub_sections->type.'-'.$sub_sections->name}}','{{$sub_sections->id}}')" style="margin-right: 5px" href="" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
      <a hidden id="{{$sub_sections->id}}" href="" class="btn btn-primary btn-sm"><i class="fas fa-trash-alt"></i></a>
    </div> 
  </span>
    @if ($sub_sections->sections)
      <ul>
        @if(count($sub_sections->sections) > 0)
            @foreach ($sub_sections->sections as $subSections)
                @include('pages.department.sub_sections', ['sub_sections' => $subSections])
            @endforeach
        @endif
      </ul>
    @endif
</li>

<script>

</script>