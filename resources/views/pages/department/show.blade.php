@extends("layouts.main")

@section('title')
    Brands
@endsection

@section("main-body")

<h1 class="page-heading rounded">Brand - {{$brand->name}}</h1>

<div class="row" id="filter-panel">
    <div class="col-12">
        <div class="my-3 p-3 bg-body rounded shadow-sm">
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <p for="name" class="form-label"><b>Name :</b> {{ $brand->name }}</p>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <p for="description" class="form-label"><b>Description :</b> {{ $brand->description }}</p>
                    </div>
                </div>
                <div class="row border-top-1 m-0 pt-4 mt-2">
                    <div class="col-12 col-md-3 offset-md-9 d-flex">
                        <a href="{{ route('brands.index') }}" class="btn btn-primary-inverse w-100 mx-1">BACK</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
