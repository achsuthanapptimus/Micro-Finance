@extends("layouts.app")

@section('title')
    Update Party
@endsection

@section('content')
    <h1 class="page-heading rounded">Create Party</h1>
    <form novalidate action="{{ route('parties.update', $user->id) }}" method="POST" enctype='multipart/form-data'>
        @csrf
        @method("PUT")
        <div class="row" id="filter-panel">
            <div class="col-12">
                <div class="my-3 p-3 bg-body rounded shadow-sm">
                    <div class="row">

                        <div class="col-12 col-md-4">
                            <div class="mb-4">
                                <label for="type" class="form-label">Type <strong style="color:red">*</strong></label>
                                    <select class="form-control mx-1 @error('type') is-invalid @enderror" name="type">
                                        <option {{ $party->type==='Customer' ? 'selected' : ''}} value="Customer">Customer</option>
                                        <option {{ $party->type==='Supplier' ? 'selected' : ''}} value="Supplier">Supplier</option>
                                    </select>
                                @error('type')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="mb-4">
                                <label for="identifier_value" class="form-label">Nic Number<strong style="color:red">*</strong></label>
                                <input class="form-control mx-1 @error('identifier_value') is-invalid @enderror"
                                value="{{ old('identifier_value', $user->identifier_value) }}" placeholder="Identifier Value" type="text" name="identifier_value" />
                                @error('identifier_value')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="mb-4">
                                <label for="title" class="form-label">Title <strong style="color:red">*</strong></label>
                                    <select class="form-control mx-1 @error('title') is-invalid @enderror" name="title">
                                        <option {{ $user->title==='Mr' ? 'selected' : ''}} value="Mr">Mr</option>
                                        <option {{ $user->title==='Mrs' ? 'selected' : ''}} value="Mrs">Mrs</option>
                                        <option {{ $user->title==='Master' ? 'selected' : ''}} value="Master">Master</option>
                                        <option {{ $user->title==='Mister' ? 'selected' : ''}} value="Mister">Mister</option>
                                        <option {{ $user->title==='Miss' ? 'selected' : ''}} value="Miss">Miss</option>
                                    </select>
                                @error('title')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="mb-4">
                                <label for="first_name" class="form-label">First Name <strong style="color:red">*</strong></label>
                                <input class="form-control mx-1 @error('first_name') is-invalid @enderror"
                                value="{{ old('first_name', $user->first_name) }}" placeholder="First Name " type="text" name="first_name" />
                                @error('first_name')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="mb-4">
                                <label for="middle_name" class="form-label">Middle Name </label>
                                <input class="form-control mx-1 @error('middle_name') is-invalid @enderror"
                                    value="{{ old('middle_name',$user->middle_name) }}" placeholder="Middle Name " type="text" name="middle_name" />
                                @error('middle_name')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="mb-4">
                                <label for="last_name" class="form-label">Last Name <strong style="color:red">*</strong></label>
                                <input class="form-control mx-1 @error('last_name') is-invalid @enderror"
                                    value="{{ old('last_name',$user->last_name) }}" placeholder="Last Name" type="text" name="last_name" />
                                @error('last_name')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="mb-4">
                                <label for="dob" class="form-label">Date Of Birth<strong style="color:red">*</strong></label>
                                <input class="form-control mx-1 @error('dob') is-invalid @enderror"
                                value="{{ old('dob',$user->dob) }}" placeholder="DOB" type="date" name="dob" />
                                @error('dob')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label for="dp" class="form-label">Dp</label>
                                <input class="form-control mx-1 @error('dp') is-invalid @enderror"
                                value="{{ old('dp') }}" placeholder="dp Address" type="file" name="dp" />
                                @error('dp')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="mb-4">
                                <label for="gender" class="form-label">Gender</strong></label>
                                    <select class="form-control mx-1 @error('title') is-invalid @enderror" name="gender">
                                        <option {{ $user->gender==='Male' ? 'selected' : ''}} value="Male">Male</option>
                                        <option {{ $user->gender==='Female' ? 'selected' : ''}} value="Female">Female</option>
                                    </select>
                                @error('gender')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="mb-4">
                                <label for="maritial_status" class="form-label">Martial Status<strong style="color:red">*</strong></label>
                                    <select class="form-control mx-1 @error('maritial_status') is-invalid @enderror" name="maritial_status">
                                        <option {{ $user->maritial_status==='Single' ? 'selected' : ''}} value="Single">Single</option>
                                        <option {{ $user->maritial_status==='Married' ? 'selected' : ''}} value="Married">Married</option>
                                        <option {{ $user->maritial_status==='Separated' ? 'selected' : ''}} value="Separated">Separated</option>
                                        <option {{ $user->maritial_status==='Widowed' ? 'selected' : ''}} value="Widowed">Widowed</option>
                                    </select>
                                @error('maritial_status')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label for="nationality" class="form-label">Nationality <strong style="color:red">*</strong></label>
                                <input class="form-control mx-1 @error('nationality') is-invalid @enderror"
                                    value="{{ old('nationality',$user->nationality) }}" placeholder="Nationality" type="text" name="nationality" />
                                @error('nationality')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label for="contact_primary" class="form-label">Phone Number<strong style="color:red">*</strong></label>
                                <input class="form-control mx-1 @error('contact_primary') is-invalid @enderror"
                                    value="{{ old('contact_primary',$user->contact_primary) }}" placeholder="Phone Number" type="text" name="contact_primary" />
                                @error('contact_primary')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label for="email" class="form-label">Email Address<strong style="color:red">*</strong></label>
                                <input class="form-control mx-1 @error('email') is-invalid @enderror"
                                value="{{ old('email',$user->email) }}" placeholder="Email Address" type="email" name="email" />
                                @error('email')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label for="province" class="form-label">Province<strong style="color:red">*</strong></label>
                                <input class="form-control mx-1 @error('province') is-invalid @enderror"
                                value="{{ old('province',$address->province) }}" placeholder="Province" type="text" name="province" />
                                @error('province')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label for="district" class="form-label">District<strong style="color:red">*</strong></label>
                                <input class="form-control mx-1 @error('district') is-invalid @enderror"
                                value="{{ old('district',$address->district) }}" placeholder="District" type="text" name="district" />
                                @error('province')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label for="city" class="form-label">City<strong style="color:red">*</strong></label>
                                <input class="form-control mx-1 @error('city') is-invalid @enderror"
                                value="{{ old('city',$address->city) }}" placeholder="City" type="text" name="city" />
                             
                                @error('city')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label for="gsdivision" class="form-label">GS Division<strong style="color:red">*</strong></label>
                                <input class="form-control mx-1 @error('gsdivision') is-invalid @enderror"
                                value="{{ old('gsdivision',$address->gsdivision) }}" placeholder="GS Division" type="text" name="gsdivision" />
                             
                                @error('gsdivision')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label for="residential_address " class="form-label">Residential Address<strong style="color:red">*</strong></label>
                                 <textarea class="form-control mx-1 @error('residential_address') is-invalid @enderror" placeholder="Residential Address" type="text"
                                    name="residential_address">{{ old('residential_address',$address->residential_address) }}</textarea>
                                @error('residential_address')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label for="official_address " class="form-label">Official Address<strong style="color:red">*</strong></label>
                                 <textarea class="form-control mx-1 @error('official_address') is-invalid @enderror" placeholder="Official Address" type="text"
                                    name="official_address">{{ old('official_address',$address->official_address) }}</textarea>
                                @error('official_address')
                                    <span class="error invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                    </div>
                    <div class="row border-top-1 m-0 pt-4 mt-2">
                        <div class="col-12 col-md-3 offset-md-9 d-flex">
                            <button type="button" onclick="window.location.replace(`{{ route('parties.index') }}`)"
                                class="btn btn-primary-inverse w-100 mx-1">BACK</button>
                            <button type="submit" class="btn btn-primary w-100 mx-1">SUBMIT</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script>
    </script>
@endsection
