@extends("layouts.app")

@section("title")
    <title>View Party</title>
@endsection

@section('content')
<h1 class="page-heading rounded">View Party</h1>

<div class="row" id="filter-panel">
    <div class="col-12">
        <div class="my-3 p-3 bg-body rounded shadow-sm">
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <label for="type" class="form-label">Type</label>
                        <p>{{$party->type }}</p>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <label for="identifier_value" class="form-label">Nic Number</label>
                        <p>{{$user->identifier_value}}</p>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <label for="identifier_value" class="form-label">Nic Number</label>
                        <p>{{$user->identifier_value}}</p>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <label for="title" class="form-label">Title</label>
                        <p>{{$user->title }}</p>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <label for="first_name" class="form-label">First Name</label>
                        <p>{{$user->first_name }}</p>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <label for="middle_name" class="form-label">Middle Name</label>
                        <p>{{$user->middle_name}}</p>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <label for="last_name" class="form-label">Last Name</label>
                        <p>{{$user->last_name}}</p>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <label for="dob" class="form-label">DOB</label>
                        <p>{{$user->dob}}</p>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <label for="gender" class="form-label">Gender</label>
                        <p>{{$user->gender}}</p>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <label for="maritial_status" class="form-label">Martial Status</label>
                        <p>{{ $user->maritial_status }}</p>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <label for="nationality" class="form-label">Nationality</label>
                        <p>{{ $user->nationality }}</p>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <label for="contact_primary" class="form-label">Phone Number</label>
                        <p>{{ $user->contact_primary }}</p>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <label for="email" class="form-label">Email Address</label>
                        <p>{{ $user->email }}</p>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <label for="email" class="form-label">Province</label>
                        <p>{{ $address->province }}</p>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <label for="district" class="form-label">District</label>
                        <p>{{ $address->district }}</p>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <label for="city" class="form-label">City</label>
                        <p>{{ $address->city }}</p>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <label for="gsdivision" class="form-label">GS Division</label>
                        <p>{{ $address->gsdivision }}</p>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <label for="residential_address" class="form-label">Residential Address</label>
                        <p>{{ $address->residential_address }}</p>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="mb-3">
                        <label for="official_address" class="form-label">Official Address</label>
                        <p>{{ $address->official_address }}</p>
                    </div>
                </div>

            </div>
            <div class="col-md-4 offset-md-8 col-12 mt-4">
                <button type="button" onclick="window.location.replace(`{{ route('parties.index') }}`)" class="btn btn-primary-inverse w-100 mx-1">BACK</button>
            </div>
        </div>
    </div>
</div>                    
@endsection
