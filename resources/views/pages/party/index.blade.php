@extends("layouts.app")

@section('title')
    Parties
@endsection

@section("content")

<form novalidate action="{{ route('parties.index') }}" method="GET" onsubmit="$('.page-loader').show()">
    <div class="row">
        <div class="col-md-4">
            <h1 class="page-heading rounded">Parties <span class="badge rounded-pill bg-warning text-white ms-3">{{$parties->total()}}</span></h1>
        </div>
        <div class="col-md-8">
            <div class="filter-cont">
                <div class="row">
                    <div class="col-md-6 col-6 d-flex search-box">
                        <input type="text"  class="form-control" name="search" value="{{$search}}" placeholder="Search"/>
                        <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                    </div>
                    <div class="col-md-2 col-4">
                        <button type="button" class="btn btn-primary btn-primary-inverse mt-2 mt-md-0" id="btn-filter">
                            <i class="fas fa-filter"></i> FILTER
                        </button>
                    </div>
                    <div class="col-md-3 col-3">
                        <a href="{{route('parties.create')}}" class="btn btn-primary btn-primary-inverse float-end">
                            <i class="fas fa-plus mx-1"></i> <span class="">ADD NEW</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="display:none" id="filter-panel">
        <div class="col-12">
            <div class="my-3 p-3 bg-body rounded shadow-sm">
                @include('include.filter',['sortBy' => $sortBy,'sortDirection' => $sortDirection,'pagePer' => $pagePer,
                    'sortByArray' => ['name' => "Name" , 'description' => "Description"]])
                <div class="row border-top-1 pt-4 mt-2">
                    <div class="col-12 col-md-3 offset-md-9 d-flex">
                        <button type="reset" class="btn btn-primary-inverse w-100 mx-1">RESET</button>
                        <button type="submit" class="btn btn-primary w-100 mx-1">SUBMIT</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="row mt-1">
    <div class="col-12">
        <div class="my-3 p-1 px-3 bg-white rounded shadow-md ">
            <table class="table table-mobile card-row data-table" id="data-table">
                <thead>
                    <tr id="thead-data">
                        <th class="col-xs">Name</th>
                        <th class="col-xs">Contact No</th>
                        <th class="col-xs">Email</th>
                        <th class="col-xs">Type</th>
                        <th class="col-xs no-sort">Actions</th>
                    </tr>
                </thead>
                <tbody id="tbody-data">
                    @foreach ($parties as $obj)
                        <tr>
                            <td>{{$obj->title}} {{'.'}} {{$obj->first_name}}</td>
                            <td>{{ $obj->contact_primary ?? "-" }}</td>
                            <td>{{ $obj->email ?? "-" }}</td>
                            <td>{{ $obj->type ?? "-" }}</td>
                            <td>
                                <div class="d-flex">
                                    <a href="{{ route('parties.show',[$obj->id]) }}"  title="View" class="btn btn-sm btn-info text-white btn-action"><i class="fas fa-eye"></i></a>
                                    <a href="{{ route('parties.edit', $obj->id) }}" title="Edit" class="btn mx-1 btn-sm btn-primary btn-action"><i class="fas fa-edit"></i></a>
                                   <form novalidate action="{{ route('parties.destroy',$obj->id) }}" class="confirm-action" title="Delete" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger  mx-1 btn-action"><i class="fas fa-trash"></i></button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="d-flex justify-content-center pagintation-cont">
                {!! $parties->appends($_GET)->links("others.pagination-tiles") !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section("scripts")
<script>
</script>
@endsection
