@extends("layouts.app")

@section('title')
    Collection Report - Detailed
@endsection

@section('content')
    <!-- FILTER FORM -->
    <form href="" method="GET">
        <h1 class="show-in-print">Collection Report - Detailed </h1>

        <div class="row hide-in-print">
            <div class="col-lg-4">
                <h1 class="page-heading">Collection Report - Detailed </h1>
                <span class="d-block mb-3 text-success"></span>
            </div>
            <div class="col-lg-8">
                <div class="p-2">
                    <div class="row">
                        <div class="col-md-8 col-8 d-flex search-box">
                            <input type="text"  class="form-control" name="search" id="txt-search" value=""/>
                            <i class="fas fa-search search-icon"></i>
                            <button type="submit" class="btn btn-primary">SEARCH</button>
                        </div>
                        <div class="col-md-2 col-3">
                            <button type="button" class="btn btn-primary btn-primary-inverse" id="btn-filter">
                                <i class="fas fa-filter"></i> FILTER
                            </button>
                        </div>
                        <div class="col-md-2 col-3 text-start">
                            <div class="dropdown">
                                <button class="btn btn-primary-inverse dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Export
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="?print=t" target="_blank" id="btn-print">Print</a></li>
                                    <li><a class="dropdown-item" href="#" id="btn-excel-dt">Download as Excel</a></li>
                                    <li><a class="dropdown-item" href="#" id="btn-pdf-dt">Download as PDF</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row" style="display:none" id="filter-panel">
            <div class="col-12">
                <div class="my-3 p-3 bg-body rounded shadow-sm">
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <div class="mb-3">
                                <label for="fdate" class="form-label">From Date</label>
                                <input type="date" class="form-control" name="fdate" value=""/>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="mb-3">
                                <label for="tdate" class="form-label">To Date</label>
                                <input type="date" class="form-control" name="tdate" value=""/>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="mb-3">
                                <label for="tdate" class="form-label">Customer</label>
                                <input type="text" class="form-control" name="tdate" value=""/>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="mb-3">
                                <label for="tdate" class="form-label">CSU</label>
                                <input type="text" class="form-control" name="tdate" value=""/>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="mb-3">
                                <label for="tdate" class="form-label">Collection Officer</label>
                                <input type="text" class="form-control" name="tdate" value=""/>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="mb-3">
                                <label for="tdate" class="form-label">Department</label>
                                <input type="text" class="form-control" name="tdate" value=""/>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="mb-3">
                                <label for="tdate" class="form-label">Product</label>
                                <input type="text" class="form-control" name="tdate" value=""/>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="mb-3">
                                <label for="tdate" class="form-label">Status</label>
                                <input type="text" class="form-control" name="tdate" value=""/>
                            </div>
                        </div>
                        <input type="hidden" class="form-control" name="grouping" value=""/>

                    </div>
                    <div class="row border-top-1 pt-4 mt-2">
                        <div class="col-12 col-md-3 offset-md-9 d-flex">
                            <button type="reset" class="btn btn-primary-inverse w-100 mx-1">RESET</button>
                            <button type="submit" class="btn btn-primary w-100 mx-1">SUBMIT</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- END OF FILTER FORM -->

    <div class="row">
        <div class="col-12">
            <div class="my-3 p-3 bg-body rounded shadow-sm">
                <table class="table table-hover w-100 table-mobile nowrap data-table" id="data-table">
                    <thead>
                        <tr>
                            <th class="show-in-print">#</th>
                            <th>Loan Code</th>
                            <th>Customer</th>
                            <th>Product</th>
                            <th>Collection Officer</th>
                            <th>CSU</th>
                            <th>Department</th>
                            <th class="number">Loan Amount</th>
                            <th class="number">Collected Amount</th>
                        </tr>
                    </thead>
                    <tbody id="tbody-data">
                        @php $x=0 @endphp
                        @foreach ($data as $row)
                            <tr>
                                @php $x++ @endphp
                                <td class="show-in-print">{{ $x }}</td>
                                <td>{{ $row->code }}</td>
                                <td>{{ $row->customer }}</td>
                                <td>{{ $row->product }}</td>
                                <td>{{ $row->officer }}</td>
                                <td>{{ $row->csu }}</td>
                                <td>{{ $row->department }}</td>
                                <td class="number">{{ number_format($row->amount, 2, '.', ',') }}</td>
                                <td class="number">{{ number_format($row->paid, 2, '.', ',') }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="show-in-print"></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class="number"></th>
                            <th class="number"></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script>
    populateDatatableSum();

    @if($print == "t")
        setTimeout(() => {
            window.print();
            window.close();
        }, 500);
    @else
        buildDataTable();
    @endif
</script>
@endpush

@push('head')
    <link href="{{ asset('assets/styles/tree.css') }}" rel="stylesheet" />
@endpush
