@extends("layouts.app")

@section('title')
    CSU Analysis
@endsection

@section('content')
    <!-- FILTER FORM -->
    <form href="" method="GET">
        <div class="row">
            <div class="col-lg-4">
                <h1 class="page-heading">CSU Analysis </h1>
                <span class="d-block mb-3 text-success"></span>
            </div>
            <div class="col-lg-8">
                <div class="p-2">
                    <div class="row">
                        <div class="col-md-6 col-6 d-flex search-box">
                                <select name="type"  class="form-control" id="sel-type" style="font-weight:bold;font-size:20px">
                                    <option value="Product">CSU 1</option>
                                    <option value="CSU">CSU 2</option>
                                    <option value="CSU">CSU 3</option>
                                    <option value="CSU">CSU 4</option>
                                </select>
                        </div>
                        <div class="col-md-2 col-3">
                            <button type="button" class="btn btn-primary btn-primary-inverse" id="btn-filter">
                                <i class="fas fa-filter"></i> FILTER
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row" style="display:none" id="filter-panel">
            <div class="col-12">
                <div class="my-3 p-3 bg-body rounded shadow-sm">
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <div class="mb-3">
                                <label for="fdate" class="form-label">From Date</label>
                                <input type="date" class="form-control" name="fdate" value=""/>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="mb-3">
                                <label for="tdate" class="form-label">To Date</label>
                                <input type="date" class="form-control" name="tdate" value=""/>
                            </div>
                        </div>
                    </div>
                    <div class="row border-top-1 pt-4 mt-2">
                        <div class="col-12 col-md-3 offset-md-9 d-flex">
                            <button type="reset" class="btn btn-primary-inverse w-100 mx-1">RESET</button>
                            <button type="submit" class="btn btn-primary w-100 mx-1">SUBMIT</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- END OF FILTER FORM -->

    <div class="row">
      <div class="col-lg-3 col-md-4 col-sm-6 col-12 d-tile">
        <a target="_blank" href="">
          <div class="my-2 p-3 bg-body rounded shadow-md d-flex">
            <div class="icon-dash icon-danger px-2"><i class="fas fa-file-import"></i></div>
            <div class="text-dash ms-3">
              <h1 class="m-0 my-1">483</h1>
              <p class="m-0">Total Active Loans</p>
            </div>
          </div>
        </a>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-12 d-tile">
        <a>
          <div class="my-2 p-3 bg-body rounded shadow-md d-flex">
            <div class="icon-dash icon-success px-2"><i class="fas fa-file-import"></i></div>
            <div class="text-dash ms-3">
              <h1 class="m-0 my-1">5,345,200.00</h1>
              <p class="m-0">Total Loan Amount</p>
            </div>
          </div>
        </a>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-12 d-tile">
        <a target="_blank" href="">
          <div class="my-2 p-3 bg-body rounded shadow-md d-flex">
            <div class="icon-dash icon-info px-2"><i class="fas fa-briefcase"></i></div>
            <div class="text-dash ms-3">
              <h1 class="m-0 my-1">42%</h1>
              <p class="m-0">Total Resettled</p>
            </div>
          </div>
        </a>
      </div>

      <div class="col-lg-3 col-md-4 col-sm-6 col-12 d-tile">
        <a href="">
          <div class="my-2 p-3 bg-body rounded shadow-md d-flex">
            <div class="icon-dash icon-primary px-2"><i class="fas fa-business-time"></i></div>
            <div class="text-dash ms-3">
              <h1 class="m-0 my-1">11</h1>
              <p class="m-0">NPLs</p>
            </div>
          </div>
        </a>
      </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="my-3 p-3 bg-body rounded shadow-sm">
                <div class="alert alert-info dont-hide">
                    CSU-wise Loan Count
                </div>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="chart-tab" data-bs-toggle="tab" data-bs-target="#chart" type="button" role="tab" aria-controls="chart" aria-selected="true">Chart View</button>
                    </li>
                    <li class="nav-item" role="presentation">
                    <button class="nav-link" id="table-tab" data-bs-toggle="tab" data-bs-target="#table" type="button" role="tab" aria-controls="table" aria-selected="false">Tabular View</button>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="chart" role="tabpanel" aria-labelledby="chart-tab">
                        <div id="chart-csu" style="height:300px"></div>
                    </div>
                    <div class="tab-pane fade" id="table" role="tabpanel" aria-labelledby="table-tab">
                        <table class="table table-hover w-100 table-mobile nowrap data-table" id="data-table">
                            <thead>
                                <tr>
                                    <th>Product </th>
                                    <th class="number">Loan Count</th>
                                </tr>
                            </thead>
                            <tbody id="tbody-data">
                                <tr>
                                <td>Product 1</td>
                                <td class="number">12</td>
                                </tr>
                                <tr>
                                <td>Product 2</td>
                                <td class="number">57</td>
                                </tr>
                                <tr>
                                <td>Product 3</td>
                                <td class="number">89</td>
                                </tr>
                                <tr>
                                <td>Product 4</td>
                                <td class="number">41</td>
                                </tr>
                                <tr>
                                <td>Product 5</td>
                                <td class="number">23</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="my-3 p-3 bg-body rounded shadow-sm">
                <div class="alert alert-info dont-hide">
                    Employee-wise Loan Count
                </div>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="chart-tab2" data-bs-toggle="tab" data-bs-target="#chart2" type="button" role="tab" aria-controls="chart" aria-selected="true">Chart View</button>
                    </li>
                    <li class="nav-item" role="presentation">
                    <button class="nav-link" id="table-tab2" data-bs-toggle="tab" data-bs-target="#table2" type="button" role="tab" aria-controls="table" aria-selected="false">Tabular View</button>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="chart2" role="tabpanel" aria-labelledby="chart-tab2">
                        <div id="chart-dept" style="height:300px"></div>
                    </div>
                    <div class="tab-pane fade" id="table2" role="tabpanel" aria-labelledby="table-tab2">
                        <table class="table table-hover w-100 table-mobile nowrap data-table" id="data-table">
                            <thead>
                                <tr>
                                    <th>Branch</th>
                                    <th class="number">Loan Count</th>
                                </tr>
                            </thead>
                            <tbody id="tbody-data">
                                <tr>
                                <td>Officer 1</td>
                                <td class="number">12</td>
                                </tr>
                                <tr>
                                <td>Officer 2</td>
                                <td class="number">57</td>
                                </tr>
                                <tr>
                                <td>Officer 3</td>
                                <td class="number">89</td>
                                </tr>
                                <tr>
                                <td>Officer 4</td>
                                <td class="number">41</td>
                                </tr>
                                <tr>
                                <td>Officer 5</td>
                                <td class="number">23</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="my-3 p-3 bg-body rounded shadow-sm">
                <div class="alert alert-info dont-hide">
                    Day-wise Loan Count
                </div>
                <div id="loan-count"></div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script>
    populateDatatableSum();
    buildDataTable();

    drawCsuGraph();
    drawDeptGraph();
    drawBarGraph();

    function drawCsuGraph()
    {
        // Sample data for CSUs
        const csuData = [
        Math.floor(Math.random() * 99) + 2,
        Math.floor(Math.random() * 99) + 2,
        Math.floor(Math.random() * 99) + 2,
        Math.floor(Math.random() * 99) + 2,
        Math.floor(Math.random() * 99) + 2,
        Math.floor(Math.random() * 99) + 2,
        Math.floor(Math.random() * 99) + 2,
        Math.floor(Math.random() * 99) + 2
        ];

        // Create a new chart instance
        const chart = new ApexCharts(document.querySelector("#chart-csu"), {
        series: csuData,
        labels: ["Product 1", "Product 2", "Product 3", "Product 4", "Product 5", "Product 6", "Product 7", "Product 8"],
        chart: {
            type: 'pie',
            height:300
        },
        });

        // Render the chart
        chart.render();
    }

    function drawDeptGraph()
    {
        // Sample data for CSUs
        const csuData = [
        Math.floor(Math.random() * 99) + 2,
        Math.floor(Math.random() * 99) + 2,
        Math.floor(Math.random() * 99) + 2,
        Math.floor(Math.random() * 99) + 2,
        ];

        // Create a new chart instance
        const chart = new ApexCharts(document.querySelector("#chart-dept"), {
        series: csuData,
        labels: ["Officer 1", "Officer 2", "Officer 3", "Officer 4"],
        chart: {
            type: 'pie',
            height:300
        },
        });

        // Render the chart
        chart.render();
    }

    function drawBarGraph()
    {
        var options = {
            chart: {
                type: 'bar',
                height: 400 // set the height to 400 pixels
            },
            series: [{
                name: 'Loan Count',
                data: [23, 41, 35, 51, 49, 62, 71]
            }],
            xaxis: {
                type: 'datetime',
                categories: ['2022-04-18', '2022-04-19', '2022-04-20', '2022-04-21', '2022-04-22', '2022-04-23', '2022-04-24']
            }
            }

            var chart = new ApexCharts(document.querySelector('#loan-count'), options);
            chart.render();
    }
</script>
@endpush

@push('head')
    <link href="{{ asset('assets/styles/tree.css') }}" rel="stylesheet" />
@endpush
