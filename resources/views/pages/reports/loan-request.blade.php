@extends("layouts.app")

@section('title')
    Loan Request
@endsection

@section('content')
    <h1>LoanRequest Report: {{$grouping}} </h1>
    <span class="d-block mb-3 text-success">From:  <b>{{$fdate ? $fdate : "[NOT SET]"}}</b> | To: <b>{{$tdate ? $tdate : "[NOT SET]"}}</b></span>

    <!-- FILTER FORM -->
    <form href="/reports/loan-request/{{$grouping}}?fdate={{$fdate}}&tdate={{$tdate}}&grouping={{$grouping}}" method="GET">
        <div class="row">
            <div class="col-md-4 col-8 d-flex search-box">
                <input type="text"  class="form-control" name="search" id="txt-search" value=""/>
                <i class="fas fa-search search-icon"></i>
                <button type="submit" class="btn btn-primary">SEARCH</button>
            </div>
            <div class="col-md-2 col-4">
                <button type="button" class="btn btn-primary btn-primary-inverse" id="btn-filter">
                    <i class="fas fa-filter"></i> FILTER
                </button>
            </div>
            <div class="col-md-2 col-4 d-none d-md-block">
                <select class="form-control" id="dlength-sel">
                    <option value="20">20 Rows</option>
                    <option value="50">50 Rows</option>
                    <option value="100">100 Rows</option>
                </select>
            </div>
        </div>

        
        <div class="row" style="display:none" id="filter-panel">
            <div class="col-12">
                <div class="my-3 p-3 bg-body rounded shadow-sm">
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <div class="mb-3">
                                <label for="fdate" class="form-label">From Date</label>
                                <input type="date" class="form-control" name="fdate" value="{{$fdate}}"/>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="mb-3">
                                <label for="tdate" class="form-label">To Date</label>
                                <input type="date" class="form-control" name="tdate" value="{{$tdate}}"/>
                            </div>
                        </div>
                        <input type="hidden" class="form-control" name="grouping" value="{{$grouping}}"/>

                    </div>
                    <div class="row border-top-1 pt-4 mt-2">
                        <div class="col-12 col-md-3 offset-md-9 d-flex">
                            <button type="reset" class="btn btn-primary-inverse w-100 mx-1">RESET</button>
                            <button type="submit" class="btn btn-primary w-100 mx-1">SUBMIT</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- END OF FILTER FORM -->

    <div class="row">
        <div class="col-12">
            <div class="my-3 p-3 bg-body rounded shadow-sm">
                <table class="table table-hover w-100 table-mobile data-table" id="data-table">
                    <thead id="thead-data">
                        <tr>
                            <th class="">{{$grouping}}</th>
                            <th class="number">LoanRequest Count</th>
                            <th class="number">LoanRequest Amount</th>
                        </tr>
                    </thead>
                    <tbody id="tbody-data">
                        @foreach($loanRequest as $row)
                            <tr>
                                <td>{{$row->grp}}</td>
                                <td  class="number">{{$row->val}}</td>
                                <td  class="number">Rs {{$row->amount}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-12">
            <div class="my-3 p-3 bg-body rounded shadow-sm">
                <div id="chart"></div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script>
    buildDataTable();
</script>

<script>
    var options = {
      series: [{
      data: @json($yaxisVals)
    }],
      chart: {
      type: 'bar',
      height: 350
    },
    plotOptions: {
      bar: {
        borderRadius: 4,
        horizontal: false,
      }
    },
    dataLabels: {
      enabled: false
    },
    xaxis: {
      categories: @json($xaxisVals)
    }
    };

    var chart = new ApexCharts(document.querySelector("#chart"), options);
    chart.render();
</script>
@endpush

@push('head')
    <link href="{{ asset('assets/styles/tree.css') }}" rel="stylesheet" />
@endpush
