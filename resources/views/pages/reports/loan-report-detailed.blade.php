@extends("layouts.app")

@section('title')
    Loan Report - Detailed
@endsection

@section('content')
    <!-- FILTER FORM -->
    <form href="" method="GET">
        <h1 class="show-in-print">Loan Report - Detailed </h1>

        <div class="row hide-in-print">
            <div class="col-lg-4">
                <h1 class="page-heading">Loan Report - Detailed </h1>
                <span class="d-block mb-3 text-success"></span>
            </div>
            <div class="col-lg-8">
                <div class="p-2">
                    <div class="row">
                        <div class="col-md-6 col-6 d-flex search-box">
                            <input type="text"  class="form-control" name="search" id="txt-search" value=""/>
                            <i class="fas fa-search search-icon"></i>
                            <button type="submit" class="btn btn-primary">SEARCH</button>
                        </div>
                        <div class="col-md-2 col-3">
                            <button type="button" class="btn btn-primary btn-primary-inverse" id="btn-filter">
                                <i class="fas fa-filter"></i> FILTER
                            </button>
                        </div>
                        <div class="col-md-3 col-3 text-start">
                            <div class="dropdown">
                                <button class="btn btn-primary-inverse dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Export
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="?print=t" target="_blank" id="btn-print">Print</a></li>
                                    <li><a class="dropdown-item" href="#" id="btn-excel-dt">Download as Excel</a></li>
                                    <li><a class="dropdown-item" href="#" id="btn-pdf-dt">Download as PDF</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row" style="display:none" id="filter-panel">
            <div class="col-12">
                <div class="my-3 p-3 bg-body rounded shadow-sm">
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <div class="mb-3">
                                <label for="fdate" class="form-label">From Date</label>
                                <input type="date" class="form-control" name="fdate" value="{{$selectedFilters->fdate}}"/>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="mb-3">
                                <label for="tdate" class="form-label">To Date</label>
                                <input type="date" class="form-control" name="tdate" value=""/>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="mb-3">
                                <label for="tdate" class="form-label">CSU</label>
                                <input type="text" class="form-control" name="tdate" value=""/>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="mb-3">
                                <label for="tdate" class="form-label">Department</label>
                                <select class="form-control" name="department">
                                    <option>All</option>
                                    @foreach ($filterData->departments as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="mb-3">
                                <label for="tdate" class="form-label">Status</label>
                                <select class="form-control" name="status">
                                    <option>All</option>
                                    @foreach ($filterData->status as $row)
                                    <option value="{{$row->status}}" {{$selectedFilters->status == $row->status ? "selected" : ""}}>{{$row->status}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <input type="hidden" class="form-control" name="grouping" value=""/>

                    </div>
                    <div class="row border-top-1 pt-4 mt-2">
                        <div class="col-12 col-md-3 offset-md-9 d-flex">
                            <button type="reset" class="btn btn-primary-inverse w-100 mx-1">RESET</button>
                            <button type="submit" class="btn btn-primary w-100 mx-1">SUBMIT</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- END OF FILTER FORM -->

    <div class="row">
        <div class="col-12">
            <div class="my-3 p-3 bg-body rounded shadow-sm">
                <table class="table table-hover w-100 table-mobile nowrap data-table" id="data-table">
                    <thead>
                        <tr>
                            <th class="show-in-print">#</th>
                            <th>Code</th>
                            <th>Customer</th>
                            <th>Product</th>
                            <th>Department</th>
                            <th>CSU</th>
                            <th class="number">Amount</th>
                            <th>Created</th>
                            <th>Status</th>
                            <th class="number">Fees</th>
                            <th class="number">Security Fund</th>
                            <th class="number">Released</th>
                            <th class="number">Rental</th>
                            <th class="number">Total Amount</th>
                            <th class="number">Paid</th>
                            <th class="number">Balance</th>
                        </tr>
                    </thead>
                    <tbody id="tbody-data">
                        @php $x=0 @endphp
                        @foreach ($data as $row)
                            <tr>
                                @php $x++ @endphp
                                <td class="show-in-print">{{ $x }}</td>
                                <td>{{ $row->code }}</td>
                                <td>{{ $row->customer }}</td>
                                <td>{{ $row->product }}</td>
                                <td>{{ $row->department }}</td>
                                <td>{{ $row->csu }}</td>
                                <td class="number">{{ number_format($row->amount, 2, '.', ',') }}</td>
                                <td>{{ $row->created }}</td>
                                <td>{{ $row->status }}</td>
                                <td class="number">{{ number_format($row->fees, 2, '.', ',') }}</td>
                                <td class="number">{{ number_format($row->sec_fund, 2, '.', ',') }}</td>
                                <td class="number">{{ number_format(($row->released - $row->fees - $row->sec_fund) , 2, '.', ',') }}</td>
                                <td class="number">{{ number_format($row->rental, 2, '.', ',') }}</td>
                                <td class="number">{{ number_format($row->payable, 2, '.', ',') }}</td>
                                <td class="number">{{ number_format($row->paid, 2, '.', ',') }}</td>
                                <td class="number">{{ number_format($row->payable - $row->paid, 2, '.', ',') }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="show-in-print"></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class="number"></th>
                            <th></th>
                            <th></th>
                            <th class="number"></th>
                            <th class="number"></th>
                            <th class="number"></th>
                            <th class="number"></th>
                            <th class="number"></th>
                            <th class="number"></th>
                            <th class="number"></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script>
    populateDatatableSum();

    @if($print == "t")
        setTimeout(() => {
            window.print();
            window.close();
        }, 500);
    @else
        buildDataTable();
    @endif
</script>
@endpush

@push('head')
    <link href="{{ asset('assets/styles/tree.css') }}" rel="stylesheet" />
@endpush
