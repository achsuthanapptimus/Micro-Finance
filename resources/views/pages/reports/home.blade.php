@extends("layouts.app")

@section('title')
    Reports
@endsection

@section('content')
        <h1>Reports Home</h1>

        <form action="" method="GET" id="report-form">
            <div class="row">
                <div class="col-12">
                    <div class="my-3 p-3 bg-body rounded shadow-sm">
                        <div class="row">
                            <div class="col-12 col-md-3">
                                <div class="mb-3">
                                    <label for="fields" class="form-label">From Date</label>
                                    <input type="date" class="form-control" name="fdate" value="{{$fdate}}" onchange="$('#report-form').trigger('submit')"/>
                                </div>
                            </div>
                            <div class="col-12 col-md-3">
                                <div class="mb-3">
                                    <label for="exampleFormControlInput1" class="form-label">To Date</label>
                                    <input type="date" class="form-control" name="tdate" value="{{$tdate}}" onchange="$('#report-form').trigger('submit')"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
             <div class="my-3 p-3 bg-body rounded shadow-sm">
                
            <a role="tab"  class="btn-shadow btn btn-primary" id="defaultOpen" data-toggle="tab" href="#loan" onclick="Report(event, 'Loan')" >
            <span>Loan</span>
            </a>

            <a role="tab"  class="btn-shadow btn btn-primary"  data-toggle="tab" href="#savings" onclick="Report(event, 'Savings')">
            <span>Savings</span>
            </a>

            <a role="tab"  class="btn-shadow btn btn-primary"  data-toggle="tab" href="#investments" onclick="Report(event, 'Investment')">
            <span>Investments</span>
            </a>

            <a role="tab"  class="btn-shadow btn btn-primary"  data-toggle="tab" href="#collections" onclick="Report(event, 'Collections')">
            <span>Collections</span>
            </a>
                
            </div> 
            <div  class="my-3 p-3 bg-body rounded shadow-sm">

            <div id="Loan" class="tabcontent">
                <div class="row">
                    <div class="col-12">
                        <div class="my-3 p-3 bg-body rounded shadow-sm">
                            <table class="table table-hover table-mobile scrollmenu horizontal-scrollbar"  id="data-table">
                                <thead>
                                    <tr id="thead-data">
                                        <th class="" >Metric</th>
                                        <th class="">Requested</th>
                                        <th class="">Created</th>
                                        <th class="">Released</th>
                                        <th class="">Repaid Amounts</th>
                                        <th class="">Interest Accumulated</th>
                                        
                                    </tr>
                                </thead>
                                <tbody id="tbody-data">
                                    <tr id="thead-data">
                                        <td class="font-weight-bold">Day-wise</td>
                                        <td><a target="_blank" href="/reports/loan-request?fdate={{$fdate}}&tdate={{$tdate}}&grouping=day-wise" class="btn btn-info text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/loan-created?fdate={{$fdate}}&tdate={{$tdate}}&grouping=day-wise" class="btn btn-warning text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/loan-released?fdate={{$fdate}}&tdate={{$tdate}}&grouping=day-wise" class="btn btn-danger text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/repaid-amounts?fdate={{$fdate}}&tdate={{$tdate}}&grouping=day-wise" class="btn btn-dark text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/interest-accumulated?fdate={{$fdate}}&tdate={{$tdate}}&grouping=day-wise" class="btn btn-info text-white">View Report</a></td>
                                        
                                    </tr>
                                    <tr id="thead-data">
                                        <td class="font-weight-bold">Month-wise</td>
                                        <td><a target="_blank" href="/reports/loan-request?fdate={{$fdate}}&tdate={{$tdate}}&grouping=month-wise" class="btn btn-info text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/loan-created?fdate={{$fdate}}&tdate={{$tdate}}&grouping=month-wise" class="btn btn-warning text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/loan-released?fdate={{$fdate}}&tdate={{$tdate}}&grouping=month-wise" class="btn btn-danger text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/repaid-amounts?fdate={{$fdate}}&tdate={{$tdate}}&grouping=month-wise" class="btn btn-dark text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/interest-accumulated?fdate={{$fdate}}&tdate={{$tdate}}&grouping=month-wise" class="btn btn-info text-white">View Report</a></td>                    
                                        
                                    </tr>
                                    <tr id="thead-data">
                                        <td class="font-weight-bold">Year-wise</td>
                                        <td><a target="_blank" href="/reports/loan-request?fdate={{$fdate}}&tdate={{$tdate}}&grouping=year-wise" class="btn btn-info text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/loan-created?fdate={{$fdate}}&tdate={{$tdate}}&grouping=year-wise" class="btn btn-warning text-white">View Report </a></td>
                                        <td><a target="_blank" href="/reports/loan-released?fdate={{$fdate}}&tdate={{$tdate}}&grouping=year-wise" class="btn btn-danger text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/repaid-amounts?fdate={{$fdate}}&tdate={{$tdate}}&grouping=year-wise" class="btn btn-dark text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/interest-accumulated?fdate={{$fdate}}&tdate={{$tdate}}&grouping=year-wise" class="btn btn-info text-white">View Report</a></td>
                                        
                                    </tr>
                                <tr id="thead-data">
                                        <td class="font-weight-bold">Department-wise</td>
                                        <td><a target="_blank" href="/reports/loan-request?fdate={{$fdate}}&tdate={{$tdate}}&grouping=department-wise" class="btn btn-info text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/loan-created?fdate={{$fdate}}&tdate={{$tdate}}&grouping=department-wise" class="btn btn-warning text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/loan-released?fdate={{$fdate}}&tdate={{$tdate}}&grouping=department-wise" class="btn btn-danger text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/repaid-amounts?fdate={{$fdate}}&tdate={{$tdate}}&grouping=department-wise" class="btn btn-dark text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/interest-accumulated?fdate={{$fdate}}&tdate={{$tdate}}&grouping=department-wise" class="btn btn-info text-white">View Report</a></td>
                                        
                                    </tr>
    
                                    <tr id="thead-data">
                                        <td class="font-weight-bold">Account-wise</td>
                                        <td></td>
                                        {{-- <td> <a target="_blank" class="btn btn-info text-white">View Report</a> </td> --}}
                                        <td><a target="_blank" href="/reports/loan-created?fdate={{$fdate}}&tdate={{$tdate}}&grouping=account-wise" class="btn btn-warning text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/loan-released?fdate={{$fdate}}&tdate={{$tdate}}&grouping=account-wise" class="btn btn-danger text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/repaid-amounts?fdate={{$fdate}}&tdate={{$tdate}}&grouping=account-wise" class="btn btn-dark text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/interest-accumulated?fdate={{$fdate}}&tdate={{$tdate}}&grouping=account-wise" class="btn btn-info text-white">View Report</a></td>
                                        
                                    </tr>
    
                                    </tr><tr id="thead-data">
                                        <td class="font-weight-bold">Customer-wise</td>
                                        <td><a target="_blank" href="/reports/loan-request?fdate={{$fdate}}&tdate={{$tdate}}&grouping=customer-wise" class="btn btn-info text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/loan-created?fdate={{$fdate}}&tdate={{$tdate}}&grouping=customer-wise" class="btn btn-warning text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/loan-released?fdate={{$fdate}}&tdate={{$tdate}}&grouping=customer-wise" class="btn btn-danger text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/repaid-amounts?fdate={{$fdate}}&tdate={{$tdate}}&grouping=customer-wise" class="btn btn-dark text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/interest-accumulated?fdate={{$fdate}}&tdate={{$tdate}}&grouping=customer-wise" class="btn btn-info text-white">View Report</a></td>
                                        
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
           
            </div>

            <div id="Savings" class="tabcontent">
                <div class="row">
                    <div class="col-12">
                        <div class="my-3 p-3 bg-body rounded shadow-sm">
                            <table class="table table-hover table-mobile scrollmenu horizontal-scrollbar" id="data-table">
                                <thead>
                                    <tr id="thead-data">
                                        <th class="">Metric</th>
                                        <th class="">Account opened</th>
                                        <th class="">Account closed</th>
                                        <th class="">Withdrawal</th>
                                        <th class="">Deposits</th>
                                        <th class="">Interest Given</th>
                                    </tr>
                                </thead>
    
                                <tbody id="tbody-data">
                                    <tr id="thead-data">
                                        <td class="font-weight-bold">Day-wise</td>
                                        <td><a target="_blank" href="/reports/saving-account-opened?fdate={{$fdate}}&tdate={{$tdate}}&grouping=day-wise" class="btn btn-warning text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/saving-account-closed?fdate={{$fdate}}&tdate={{$tdate}}&grouping=day-wise" class="btn btn-danger text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/withdrawals?fdate={{$fdate}}&tdate={{$tdate}}&grouping=day-wise" class="btn btn-dark text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/deposits?fdate={{$fdate}}&tdate={{$tdate}}&grouping=day-wise" class="btn btn-info text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/interest-given?fdate={{$fdate}}&tdate={{$tdate}}&grouping=day-wise" class="btn btn-warning text-white">View Report</a></td>
                                        
                                    </tr>
                                    <tr id="thead-data">
                                        <td class="font-weight-bold">Month-wise</td>
                                        <td><a target="_blank" href="/reports/saving-account-opened?fdate={{$fdate}}&tdate={{$tdate}}&grouping=month-wise" class="btn btn-warning text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/saving-account-closed?fdate={{$fdate}}&tdate={{$tdate}}&grouping=month-wise" class="btn btn-danger text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/withdrawals?fdate={{$fdate}}&tdate={{$tdate}}&grouping=month-wise" class="btn btn-dark text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/deposits?fdate={{$fdate}}&tdate={{$tdate}}&grouping=month-wise" class="btn btn-info text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/interest-given?fdate={{$fdate}}&tdate={{$tdate}}&grouping=month-wise" class="btn btn-warning text-white">View Report</a></td>
                                        
                                    </tr>
                                    <tr id="thead-data">
                                        <td class="font-weight-bold">Year-wise</td>
                                        <td><a target="_blank" href="/reports/saving-account-opened?fdate={{$fdate}}&tdate={{$tdate}}&grouping=year-wise" class="btn btn-warning text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/saving-account-closed?fdate={{$fdate}}&tdate={{$tdate}}&grouping=year-wise" class="btn btn-danger text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/withdrawals?fdate={{$fdate}}&tdate={{$tdate}}&grouping=year-wise" class="btn btn-dark text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/deposits?fdate={{$fdate}}&tdate={{$tdate}}&grouping=year-wise" class="btn btn-info text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/interest-given?fdate={{$fdate}}&tdate={{$tdate}}&grouping=year-wise" class="btn btn-warning text-white">View Report</a></td>
                                        
                                    </tr>
                                    <tr id="thead-data">
                                        <td class="font-weight-bold">Department-wise </td>
                                        <td><a target="_blank" href="/reports/saving-account-opened?fdate={{$fdate}}&tdate={{$tdate}}&grouping=department-wise" class="btn btn-warning text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/saving-account-closed?fdate={{$fdate}}&tdate={{$tdate}}&grouping=department-wise" class="btn btn-danger text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/withdrawals?fdate={{$fdate}}&tdate={{$tdate}}&grouping=department-wise" class="btn btn-dark text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/deposits?fdate={{$fdate}}&tdate={{$tdate}}&grouping=department-wise" class="btn btn-info text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/interest-given?fdate={{$fdate}}&tdate={{$tdate}}&grouping=department-wise" class="btn btn-warning text-white">View Report</a></td>
                                        
                                    </tr>
    
                                    <tr id="thead-data">
                                        <td class="font-weight-bold">Account-wise</td>
                                        <td><a target="_blank" href="/reports/saving-account-opened?fdate={{$fdate}}&tdate={{$tdate}}&grouping=account-wise" class="btn btn-warning text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/saving-account-closed?fdate={{$fdate}}&tdate={{$tdate}}&grouping=account-wise" class="btn btn-danger text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/withdrawals?fdate={{$fdate}}&tdate={{$tdate}}&grouping=account-wise" class="btn btn-dark text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/deposits?fdate={{$fdate}}&tdate={{$tdate}}&grouping=account-wise" class="btn btn-info text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/interest-given?fdate={{$fdate}}&tdate={{$tdate}}&grouping=account-wise" class="btn btn-warning text-white">View Report</a></td>
                                        
                                    </tr>
    
                                    </tr><tr id="thead-data">
                                        <td class="font-weight-bold">Customer-wise</td>
                                        <td><a target="_blank" href="/reports/saving-account-opened?fdate={{$fdate}}&tdate={{$tdate}}&grouping=customer-wise" class="btn btn-warning text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/saving-account-closed?fdate={{$fdate}}&tdate={{$tdate}}&grouping=customer-wise" class="btn btn-danger text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/withdrawals?fdate={{$fdate}}&tdate={{$tdate}}&grouping=customer-wise" class="btn btn-dark text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/deposits?fdate={{$fdate}}&tdate={{$tdate}}&grouping=customer-wise" class="btn btn-info text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/interest-given?fdate={{$fdate}}&tdate={{$tdate}}&grouping=customer-wise" class="btn btn-warning text-white">View Report</a></td>
                                        
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
              
            
            <div id="Investment" class="tabcontent">    
            <div class="row">
                <div class="col-12">
                    <div class="my-3 p-3 bg-body rounded shadow-sm">
                        <table class="table table-hover table-mobile scrollmenu horizontal-scrollbar " id="data-table">
                            <thead>
                                <tr id="thead-data">
                                    <th class="">Metric</th>
                                    <th class="">Created</th>
                                    {{-- <th class="">Investment Account Opened</th>  --}}
                                    <th class=""> Closed</th>
                                </tr>
                            </thead>

                            <tbody id="tbody-data">
                                <tr id="thead-data">
                                    <td class="font-weight-bold">Day-wise</td>
                                    <td><a target="_blank" href="/reports/investment-created?fdate={{$fdate}}&tdate={{$tdate}}&grouping=day-wise" class="btn btn-danger text-white">View Report </a></td>
                                    {{-- <td><a target="_blank" href="/reports/investment-account-opened?fdate={{$fdate}}&tdate={{$tdate}}&grouping=day-wise" class="btn btn-dark text-white">View Report</a></td> --}}
                                    <td><a target="_blank" href="/reports/investment-account-closed?fdate={{$fdate}}&tdate={{$tdate}}&grouping=day-wise" class="btn btn-info text-white">View Report</a></td>
                                </tr>
                                <tr id="thead-data">
                                    <td class="font-weight-bold">Month-wise</td>
                                    <td><a target="_blank" href="/reports/investment-created?fdate={{$fdate}}&tdate={{$tdate}}&grouping=month-wise" class="btn btn-danger text-white">View Report </a></td>
                                    {{-- <td><a target="_blank" href="/reports/investment-account-opened?fdate={{$fdate}}&tdate={{$tdate}}&grouping=month-wise" class="btn btn-dark text-white">View Report</a></td> --}}
                                    <td><a target="_blank" href="/reports/investment-account-closed?fdate={{$fdate}}&tdate={{$tdate}}&grouping=month-wise" class="btn btn-info text-white">View Report</a></td>
                                </tr>
                                <tr id="thead-data">
                                    <td class="font-weight-bold">Year-wise</td>
                                    <td><a target="_blank" href="/reports/investment-created?fdate={{$fdate}}&tdate={{$tdate}}&grouping=year-wise" class="btn btn-danger text-white">View Report </a></td>
                                    {{-- <td><a target="_blank" href="/reports/investment-account-opened?fdate={{$fdate}}&tdate={{$tdate}}&grouping=year-wise" class="btn btn-dark text-white">View Report</a></td> --}}
                                    <td><a target="_blank" href="/reports/investment-account-closed?fdate={{$fdate}}&tdate={{$tdate}}&grouping=year-wise" class="btn btn-info text-white">View Report</a></td>
                                </tr>
                                <tr id="thead-data">
                                    <td class="font-weight-bold">Department-wise </td>
                                    <td><a target="_blank" href="/reports/investment-created?fdate={{$fdate}}&tdate={{$tdate}}&grouping=department-wise" class="btn btn-danger text-white">View Report </a></td>
                                    {{-- <td><a target="_blank" href="/reports/investment-account-opened?fdate={{$fdate}}&tdate={{$tdate}}&grouping=department-wise" class="btn btn-dark text-white">View Report</a></td> --}}
                                    <td><a target="_blank" href="/reports/investment-account-closed?fdate={{$fdate}}&tdate={{$tdate}}&grouping=department-wise" class="btn btn-info text-white">View Report</a></td>
                                </tr>

                                <tr id="thead-data">
                                    <td class="font-weight-bold">product-wise</td>
                                    <td><a target="_blank" href="/reports/investment-created?fdate={{$fdate}}&tdate={{$tdate}}&grouping=product-wise" class="btn btn-danger text-white">View Report </a></td>
                                    {{-- <td><a target="_blank" href="/reports/investment-account-opened?fdate={{$fdate}}&tdate={{$tdate}}&grouping=account-wise" class="btn btn-dark text-white">View Report</a></td> --}}
                                    {{-- <td><a target="_blank" href="/reports/investment-account-closed?fdate={{$fdate}}&tdate={{$tdate}}&grouping=product-wise" class="btn btn-info text-white">View Report</a></td> --}}
                                </tr>

                                </tr><tr id="thead-data">
                                    <td class="font-weight-bold">Customer-wise</td>
                                    <td><a target="_blank" href="/reports/investment-created?fdate={{$fdate}}&tdate={{$tdate}}&grouping=customer-wise" class="btn btn-danger text-white">View Report </a></td>
                                    {{-- <td><a target="_blank" href="/reports/investment-account-opened?fdate={{$fdate}}&tdate={{$tdate}}&grouping=customer-wise" class="btn btn-dark text-white">View Report</a></td> --}}
                                    <td><a target="_blank" href="/reports/investment-account-closed?fdate={{$fdate}}&tdate={{$tdate}}&grouping=customer-wise" class="btn btn-info text-white">View Report</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>

            <div id="Collections" class="tabcontent">
                <div class="row">
                    <div class="col-12">
                        <div class="my-3 p-3 bg-body rounded shadow-sm">
                            <table class="table table-hover table-mobile scrollmenu horizontal-scrollbar" id="data-table">
                                <thead>
                                    <tr id="thead-data">
                                        <th class="">Officer-Wise</th>
                                        <th class="">Center-Wise</th>
                                        <th class="">Department-Wise</th>
                                        <th class="">Product-Wise</th>
                                    </tr>
                                </thead>
    
                                <tbody id="tbody-data">
                                    <tr id="thead-data">
                                        <td><a target="_blank" href="/reports/collection/officer-wise?fdate={{$fdate}}&tdate={{$tdate}}" class="btn btn-warning text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/collection/center-wise?fdate={{$fdate}}&tdate={{$tdate}}" class="btn btn-danger text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/collection/department-wise?fdate={{$fdate}}&tdate={{$tdate}}" class="btn btn-dark text-white">View Report</a></td>
                                        <td><a target="_blank" href="/reports/deposits?fdate={{$fdate}}&tdate={{$tdate}}&grouping=day-wise" class="btn btn-info text-white">View Report</a></td>                                        
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        </form>
@endsection

@push('scripts')
    <script src="{{ asset('assets/js/tree.js') }}"></script>
    <script>
       //filling attributes for td columns in mobile View Report
       $.each($('#data-table thead tr th'), function(i, th){
        if($(th).text() != "" && $(th).text() != "Actions")
            $('#data-table tbody tr td:nth-child('+ (i+1) +')').attr("label", $(th).text() + ": ");
    });

        function Report(evt, accounttype) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(accounttype).style.display = "block";
        evt.currentTarget.className += " active";
        }


         document.getElementById("defaultOpen").click();
    </script>
@endpush

@push('head')
    <link href="{{ asset('assets/styles/tree.css') }}" rel="stylesheet" />
@endpush
