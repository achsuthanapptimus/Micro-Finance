<nav class="navbar navbar-expand-lg fixed-top shadow-sm">
    <div class="container-fluid">
        <a class="navbar-brand" href="">
            <img src="/assets/images/logo.png" class="d-none d-md-inline-block" />
            <img src="/assets/images/logo-white.png" class="d-inline-block d-md-none" />
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                {{-- <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                      {{$current_department['department']}} - {{$current_department['role']}}
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @foreach ($departments as $item)
                        <li><a class="dropdown-item" href="#">{{$item['department']}} - {{$item['role']}}</a></li>
                        @endforeach
                    </ul>
                  </li> --}}
            </ul>
            <a href="/" class="btn btn-warning text-white header-icon p-2 px-3 me-3">
                <span class="far fa-home"></span> Home
            </a>
            <a class="btn float-end header-icon" href="">
                <i class="far fa-bell"></i>

            </a>
            <a class="btn float-end header-icon d-none" href="">
                <i class="far fa-comment"></i>
                @if (isset($messageCount))
                    <span class="notification-cont">
                        {{ $messageCount }}
                    </span>
                @endif
            </a>
            <a href="" class="btn header-icon">
                <i class="far fa-user"></i>
            </a>
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                Logout
            </a>    
            <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>
    </div>
</nav>
