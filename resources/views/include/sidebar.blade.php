<div class="sidebar-default mini-scrollbar shadow-sm" id="navbarsExampleDefault">
    <ul class="navbar-nav me-auto mb-2 mb-lg-0">

     
  

        <!-- ADMIN MENU ==================================================== -->
        @if (Auth::check())
            <li class="nav-item d-none"><a class="nav-link rounded"  aria-current="page" href="#loan" id="defaultOpen" onclick="Report(event, 'Loan')"><i class="fas fa-donate"></i> Loan</a></li>
            <li class="nav-item d-none"><a class="nav-link rounded"  aria-current="page" href="#savings" onclick="Report(event, 'Savings')"><i class="fas fa-hand-holding-usd" ></i>Savings</a></li>
            <li class="nav-item d-none"><a class="nav-link rounded"  aria-current="page" href="#investments" onclick="Report(event, 'Investments')"><i class="fas fa-coins"></i> Investments</a></li>
            <li class="nav-item"><a class="nav-link rounded"  aria-current="page" href="/reports/loans-report"><i class="fas fa-coins"></i>Loans Detailed</a></li>
            <li class="nav-item"><a class="nav-link rounded"  aria-current="page" href="/reports/loans-summary-report"><i class="fas fa-coins"></i>Loans Summary</a></li>
            <li class="nav-item"><a class="nav-link rounded"  aria-current="page" href="/reports/loans-combined-summary-report"><i class="fas fa-coins"></i>Combined Summary</a></li>
            <li class="nav-item"><a class="nav-link rounded"  aria-current="page" href="/reports/release-report"><i class="fas fa-coins"></i>Loan Release</a></li>
            <li class="nav-item"><a class="nav-link rounded"  aria-current="page" href="/reports/release-summary"><i class="fas fa-coins"></i>Loan Release Summary</a></li>
            <li class="nav-item"><a class="nav-link rounded"  aria-current="page" href="/reports/collection-report"><i class="fas fa-coins"></i>Collection Detailed</a></li>
            <li class="nav-item"><a class="nav-link rounded"  aria-current="page" href="/reports/collection-summary-report"><i class="fas fa-coins"></i>Collection Summary</a></li>
            <li class="nav-item"><a class="nav-link rounded"  aria-current="page" href="/reports/fees-report"><i class="fas fa-coins"></i>Fees-wise Report</a></li>
            <li class="nav-item"><a class="nav-link rounded"  aria-current="page" href="/reports/debtor-balance-report"><i class="fas fa-coins"></i>Debtor Balance</a></li>
            <li class="nav-item"><a class="nav-link rounded"  aria-current="page" href="/reports/npl-report"><i class="fas fa-coins"></i>NPLs</a></li>
            <li class="nav-item d-none"><a class="nav-link rounded"  aria-current="page" href="/reports/product-analysis-report"><i class="fas fa-coins"></i>Product Analysis</a></li>
            <li class="nav-item d-none"><a class="nav-link rounded"  aria-current="page" href="/reports/csu-analysis-report"><i class="fas fa-coins"></i>CSU Analysis</a></li>
            <li class="nav-item d-none"><a class="nav-link rounded"  aria-current="page" href="/reports/branch-analysis-report"><i class="fas fa-coins"></i>Branch Analysis</a></li>

        @endif
        <!-- *************************************************************** -->
        <!-- COMMON AUTHENTICATED USERS MENU =============================== -->
        @if (Auth::check())
            <!-- LOGOUT MENU ================================================== -->
            <li class="nav-item"><a class="nav-link rounded" aria-current="page" href="#"
                    onclick="$('#frm-logout').submit()"><i class="fas fa-sign-out-alt"></i> Logout</a></li>

            <form novalidate method="POST" action="{{ route('logout') }}" id="frm-logout">
                @csrf
            </form>
        @endif

    </ul>
</div>
